# Copyright © 2016-2017 Simon McVittie
# Copyright © 2017 Collabora Ltd.
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
import shutil
import subprocess
import textwrap
import uuid
import urllib.parse
from abc import abstractmethod, ABCMeta
from contextlib import ExitStack
from tempfile import TemporaryDirectory
from typing import (
    Dict,
    Iterable,
    Optional,
    Sequence,
    Set,
    TYPE_CHECKING,
    TextIO,
    Tuple,
    Type,
    TypeVar,
)

from debian.deb822 import (
    Changes,
    Dsc,
)
from debian.debian_support import (
    Version,
)

from vectis.apt import (
    iter_apt_sources,
)
from vectis.error import (
    Error,
)
from vectis.util import (
    AtomicWriter,
)

if TYPE_CHECKING:
    from types import (
        TracebackType,
    )

    from vectis.config import (
        ArchiveAccess,
        Suite,
    )

_WRAPPER = os.path.join(os.path.dirname(__file__), 'vectis-command-wrapper')

logger = logging.getLogger(__name__)


class WorkerError(Error):
    pass


SameBaseWorker = TypeVar('SameBaseWorker', bound='BaseWorker')


class BaseWorker(metaclass=ABCMeta):

    def __init__(
        self,
        *,
        archive_access: 'Optional[ArchiveAccess]' = None,
        suite: 'Optional[Suite]' = None
    ) -> None:
        super().__init__()
        self.__open = 0
        self.archive_access = archive_access
        self.stack = ExitStack()

    def assert_open(self) -> None:
        assert self.__open

    def __enter__(self: SameBaseWorker) -> SameBaseWorker:
        self.__open += 1

        if self.__open == 1:
            self._open()

        return self

    def _open(self) -> None:
        pass

    def __exit__(
        self,
        exc_type: 'Optional[Type[BaseException]]',
        exc_value: 'Optional[BaseException]',
        traceback: 'Optional[TracebackType]',
    ) -> Optional[bool]:
        self.__open -= 1
        if self.__open:
            return False
        else:
            try:
                return self.stack.__exit__(exc_type, exc_value, traceback)
            except BrokenPipeError:
                logger.debug('Ignoring error during teardown:', exc_info=True)
                return False


class ContainerWorker(BaseWorker, metaclass=ABCMeta):

    def __init__(
        self,
        *,
        archive_access: 'ArchiveAccess',
        suite: 'Suite',
    ) -> None:
        super().__init__(archive_access=archive_access)
        self.archive_access: ArchiveAccess = archive_access
        self.components: Sequence[str] = ()
        self.extra_repositories: Sequence[str] = ()
        self.extra_repository_keys: Sequence[str] = ()
        self.suite = suite

    @abstractmethod
    def install_apt_key(self, apt_key: str) -> None:
        raise NotImplementedError

    def install_apt_keys(self) -> None:
        assert self.suite is not None

        for ancestor in self.suite.hierarchy:
            if ancestor.apt_key is not None:
                self.install_apt_key(ancestor.apt_key)

        for key in self.extra_repository_keys:
            self.install_apt_key(key)

    @abstractmethod
    def set_up_apt(self) -> None:
        raise NotImplementedError

    def write_sources_list(self, writer: TextIO) -> None:
        assert self.archive_access is not None
        assert self.suite is not None

        for source in iter_apt_sources(
            suite=self.suite,
            archive_access=self.archive_access,
            components=self.components,
        ):
            logger.info('%r: %r', self, source)
            writer.write('{}\n'.format(source))

        for line in self.extra_repositories:
            writer.write('{}\n'.format(line))

    def write_apt_conf(self, writer: TextIO) -> None:
        hosts = set()

        for ancestor in self.suite.hierarchy:
            uri = self.archive_access.mirror_for_suite(ancestor)
            parsed = urllib.parse.urlparse(uri)
            if parsed.hostname is not None:
                hosts.add(parsed.hostname)

        for line in self.extra_repositories:
            for word in line.split():
                if word.startswith('http://') or word.startswith('https://'):
                    parsed = urllib.parse.urlparse(word)
                    hostname = parsed.hostname

                    if hostname is not None:
                        hosts.add(hostname)

        self.archive_access.write_apt_conf(writer, hosts)


class FileProvider(BaseWorker, metaclass=ABCMeta):

    @abstractmethod
    def make_file_available(
        self,
        filename: str,
        *,
        cache: bool = False,
        in_dir: Optional[str] = None,
        owner: Optional[str] = None
    ) -> str:
        raise NotImplementedError

    @abstractmethod
    def new_directory(
        self,
        prefix: str = '',
        tmpdir: Optional[str] = None,
    ) -> str:
        raise NotImplementedError

    @abstractmethod
    def make_changes_file_available(
        self,
        filename: str,
        owner: Optional[str] = None,
    ) -> Tuple[str, str]:
        raise NotImplementedError

    @abstractmethod
    def make_dsc_file_available(
        self,
        filename: str,
        owner: Optional[str] = None,
    ) -> Tuple[str, str]:
        raise NotImplementedError


class InteractiveWorker(BaseWorker, metaclass=ABCMeta):

    def __init__(
        self,
        *,
        archive_access: 'Optional[ArchiveAccess]' = None,
        suite: 'Optional[Suite]' = None
    ) -> None:
        super().__init__(archive_access=archive_access, suite=suite)
        self.__dpkg_architecture: Optional[str] = None

    def call(self, argv, **kwargs):
        raise NotImplementedError

    def check_call(self, argv, **kwargs):
        raise NotImplementedError

    def check_output(self, argv, **kwargs):
        raise NotImplementedError

    def dpkg_version(self, package: str) -> Version:
        v = self.check_output(
            ['dpkg-query', '-W', '-f${Version}', package],
            universal_newlines=True).rstrip('\n')
        return Version(v)

    @property
    def dpkg_architecture(self) -> str:
        if self.__dpkg_architecture is None:
            self.__dpkg_architecture = self.check_output(
                ['dpkg', '--print-architecture'],
                universal_newlines=True).strip()

        return self.__dpkg_architecture

    def can_run_dpkg_architecture(self, arch: str) -> bool:
        mine = self.dpkg_architecture

        if mine == arch:
            return True

        if mine == 'amd64':
            return arch in ('i386', 'x32')

        if mine == 'kfreebsd-amd64':
            return arch == 'kfreebsd-i386'

        if mine == 'armhf':
            return arch == 'armel'

        if mine == 'mips64el':
            return arch == 'mipsel'

        if mine == 'ppc64':
            return arch == 'powerpc'

        if mine == 's390x':
            return arch == 's390'

        if mine == 'sparc64':
            return arch == 'sparc'

        return False


class HostWorker(InteractiveWorker, FileProvider):

    def __init__(self) -> None:
        super().__init__()

    def call(self, argv, **kwargs):
        logger.info('%r: %r', self, argv)
        return subprocess.call(argv, **kwargs)

    def check_call(self, argv, **kwargs):
        logger.info('%r: %r', self, argv)
        subprocess.check_call(argv, **kwargs)

    def check_output(self, argv, **kwargs):
        logger.info('%r: %r', self, argv)
        return subprocess.check_output(argv, **kwargs)

    def make_file_available(
        self,
        filename: str,
        *,
        cache: bool = False,
        in_dir: Optional[str] = None,
        owner: Optional[str] = None,
    ) -> str:
        return filename

    def new_directory(
        self,
        prefix: str = '',
        tmpdir: Optional[str] = None,
    ) -> str:
        if not prefix:
            prefix = 'vectis-'

        return str(
            self.stack.enter_context(
                TemporaryDirectory(prefix=prefix)
            )
        )

    def make_dsc_file_available(
        self,
        filename,
        owner=None
    ) -> Tuple[str, str]:
        return (
            os.path.dirname(filename) or os.curdir,
            os.path.basename(filename),
        )

    def make_changes_file_available(
        self,
        filename,
        owner=None
    ) -> Tuple[str, str]:
        return (
            os.path.dirname(filename) or os.curdir,
            os.path.basename(filename),
        )


class SchrootWorker(ContainerWorker, InteractiveWorker):

    def __init__(
        self,
        *,
        architecture: str,
        archive_access: 'ArchiveAccess',
        suite: 'Suite',
        worker: 'VirtWorker',
        chroot: Optional[str] = None,
        components: Sequence[str] = (),
        extra_repositories: Sequence[str] = (),
        extra_repository_keys: Sequence[str] = (),
        storage: Optional[str] = None,
        tarball: Optional[str] = None,
    ) -> None:
        super().__init__(archive_access=archive_access, suite=suite)

        if chroot is None:
            chroot = '{}-{}-{}'.format(suite.vendor, suite, architecture)

        if tarball is None:
            tarball = 'sbuild.tar.gz'

        if '/' not in tarball:
            assert storage is not None
            tarball = os.path.join(
                storage,
                str(suite.hierarchy[-1].vendor),
                str(suite.hierarchy[-1]),
                architecture,
                tarball,
            )

        self.chroot = chroot
        self.components = components
        self.__dpkg_architecture = architecture
        self.extra_repositories = extra_repositories
        self.extra_repository_keys = extra_repository_keys
        self.tarball = tarball
        self.worker = worker

        # We currently assume that copy_to_guest() works, and that we can
        # write in /etc/schroot/
        assert isinstance(self.worker, VirtWorker)

    @property
    def dpkg_architecture(self) -> str:
        if self.__dpkg_architecture is not None:
            return self.__dpkg_architecture
        else:
            return super().dpkg_architecture

    def _open(self) -> None:
        super()._open()
        self.set_up_apt()

    def set_up_apt(self) -> None:
        tarball_in_guest = self.worker.make_file_available(
            self.tarball, cache=True)

        context = TemporaryDirectory(prefix='vectis-worker-')
        tmp = self.stack.enter_context(context)
        sources_list = os.path.join(tmp, 'sources.list')
        apt_conf = os.path.join(tmp, 'apt.conf')

        with AtomicWriter(sources_list) as writer:
            self.write_sources_list(writer)

        with AtomicWriter(apt_conf) as writer:
            self.write_apt_conf(writer)

        self.worker.check_call(['mkdir', '-p', '/etc/schroot/sources.list.d'])
        self.worker.copy_to_guest(
            sources_list,
            '/etc/schroot/sources.list.d/{}'.format(self.chroot))

        self.worker.check_call(['mkdir', '-p', '/etc/schroot/apt.conf.d'])
        self.worker.copy_to_guest(
            apt_conf,
            '/etc/schroot/apt.conf.d/{}'.format(self.chroot))

        with AtomicWriter(os.path.join(tmp, 'sbuild.conf')) as writer:
            writer.write(textwrap.dedent('''
            [{chroot}]
            type=file
            description=An autobuilder
            file={tarball_in_guest}
            groups=root,sbuild
            root-groups=root,sbuild
            profile=sbuild
            ''').format(
                chroot=self.chroot,
                tarball_in_guest=tarball_in_guest))

            if self.__dpkg_architecture in (
                'armel', 'armhf', 'i386', 'mips', 'mipsel', 'powerpc',
                's390', 'sparc',
            ):
                writer.write('personality=linux32\n')

        self.worker.copy_to_guest(
            os.path.join(tmp, 'sbuild.conf'),
            '/etc/schroot/chroot.d/{}'.format(self.chroot))

        with AtomicWriter(os.path.join(tmp, '60vectis-sources')) as writer:
            writer.write(textwrap.dedent('''\
            #!/bin/sh
            set -e
            set -u
            if [ $1 = setup-start ] || [ $1 = setup-recover ]; then
                echo "$0: Setting up ${CHROOT_ALIAS}" >&2

                if [ -f /etc/schroot/sources.list.d/${CHROOT_ALIAS} ]; then
                    echo "$0: Copying" \
                        "/etc/schroot/sources.list.d/${CHROOT_ALIAS}" \
                        "into ${CHROOT_PATH}" >&2
                    cp /etc/schroot/sources.list.d/${CHROOT_ALIAS} \
                        ${CHROOT_PATH}/etc/apt/sources.list
                fi

                if [ -f /etc/schroot/apt.conf.d/${CHROOT_ALIAS} ]; then
                    echo "$0: Copying" \
                        "/etc/schroot/apt.conf.d/${CHROOT_ALIAS}" \
                        "into ${CHROOT_PATH}" >&2
                    cp /etc/schroot/apt.conf.d/${CHROOT_ALIAS} \
                        ${CHROOT_PATH}/etc/apt/apt.conf
                    rm -f ${CHROOT_PATH}/etc/apt/apt.conf.d/01proxy
                fi

                if [ -d /etc/schroot/apt-keys.d/${CHROOT_ALIAS} ]; then
                    echo "$0: Copying" \
                        "/etc/schroot/apt-keys.d/${CHROOT_ALIAS}/" \
                        "into ${CHROOT_PATH}" >&2
                    cp /etc/schroot/apt-keys.d/${CHROOT_ALIAS}/* \
                        ${CHROOT_PATH}/etc/apt/trusted.gpg.d/
                fi
            fi
            '''))
        self.worker.copy_to_guest(
            os.path.join(tmp, '60vectis-sources'),
            '/etc/schroot/setup.d/60vectis-sources')
        self.worker.check_call(
            ['chmod', '0755', '/etc/schroot/setup.d/60vectis-sources'])
        self.install_apt_keys()

    def install_apt_key(self, apt_key) -> None:
        self.worker.check_call(
            ['mkdir', '-p', '/etc/schroot/apt-keys.d/{}'.format(self.chroot)])
        self.worker.copy_to_guest(
            apt_key, '/etc/schroot/apt-keys.d/{}/{}-{}'.format(
                self.chroot, uuid.uuid4(), os.path.basename(apt_key)))

    def call(self, argv, **kwargs):
        return self.worker.call([
            'schroot', '-c', self.chroot,
            '--',
        ] + list(argv), **kwargs)

    def check_call(self, argv, **kwargs):
        return self.worker.check_call([
            'schroot', '-c', self.chroot,
            '--',
        ] + list(argv), **kwargs)

    def check_output(self, argv, **kwargs):
        return self.worker.check_output([
            'schroot', '-c', self.chroot,
            '--',
        ] + list(argv), **kwargs)


class VirtWorker(InteractiveWorker, ContainerWorker, FileProvider):

    def __init__(
        self,
        argv: Sequence[str],
        *,
        archive_access: 'ArchiveAccess',
        storage: str,
        suite: 'Suite',
        apt_update=True,
        components: Sequence[str] = (),
        extra_repositories: Sequence[str] = (),
        extra_repository_keys: Sequence[str] = (),
    ) -> None:
        super().__init__(archive_access=archive_access, suite=suite)

        self.__cached_copies: Dict[str, str] = {}
        self.__command_wrapper_enabled = False
        self.apt_update = apt_update
        self.argv = argv
        self.call_argv: Optional[Sequence[str]] = None
        self.capabilities: Set[str] = set()
        self.command_wrapper: Optional[str] = None
        self.components = components
        self.extra_repositories = extra_repositories
        self.extra_repository_keys = extra_repository_keys
        self.user = 'user'
        self.virt_process: Optional[subprocess.Popen] = None

    def __repr__(self) -> str:
        return '<{} {}>'.format(self.__class__.__name__, self.argv)

    def _open(self) -> None:
        super()._open()
        argv = [str(os.path.expanduser(x)) for x in self.argv]

        for prefix in ('autopkgtest-virt-', 'adt-virt-', ''):
            if shutil.which(prefix + argv[0]):
                argv[0] = prefix + argv[0]
                break
        else:
            raise WorkerError('virtualization provider %r not found' % argv[0])

        logger.info('Starting worker: %r', argv)
        self.virt_process = subprocess.Popen(
            argv,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            universal_newlines=True)
        stdin = self.virt_process.stdin
        stdout = self.virt_process.stdout
        assert stdin is not None
        assert stdout is not None
        self.stack.enter_context(self.virt_process)
        self.stack.callback(self.virt_process.terminate)
        # FIXME: timed wait for a response?
        self.stack.callback(stdin.flush)
        self.stack.callback(stdin.write, 'quit\n')

        line = stdout.readline()

        if line != 'ok\n':
            raise WorkerError('Virtual machine {!r} failed to start: '
                              '{}'.format(argv, line.strip()))

        stdin.write('capabilities\n')
        stdin.flush()
        line = stdout.readline()

        if line != 'ok\n' and not line.startswith('ok '):
            raise WorkerError(
                'Virtual machine {!r} failed to report capabilities: '
                '{}'.format(argv, line.strip()))

        for word in line.split()[1:]:
            self.capabilities.add(word)
            if word.startswith('suggested-normal-user='):
                self.user = word[len('suggested-normal-user='):]

        if 'root-on-testbed' not in self.capabilities:
            raise WorkerError(
                'Virtual machine {!r} does not have root-on-testbed '
                'capability: {}'.format(argv, line.strip()))

        if ('isolation-machine' not in self.capabilities and
                'isolation-container' not in self.capabilities):
            raise WorkerError(
                'Virtual machine {!r} does not have sufficient isolation: '
                '{}'.format(argv, line.strip()))

        stdin.write('open\n')
        stdin.flush()
        line = stdout.readline()
        if not line.startswith('ok '):
            raise WorkerError(
                'Failed to open virtual machine session '
                '{!r}: {}'.format(argv, line))
        self.scratch = line[3:].rstrip('\n')

        stdin.write('print-execute-command\n')
        stdin.flush()
        line = stdout.readline()
        if not line.startswith('ok '):
            raise WorkerError(
                'Failed to get virtual machine {!r} command '
                'wrapper: {}'.format(argv, line.strip()))

        wrapper_argv = line.rstrip('\n').split(None, 1)[1].split(',')
        self.call_argv = list(map(urllib.parse.unquote, wrapper_argv))
        if not self.call_argv:
            raise WorkerError(
                'Virtual machine {!r} command wrapper did not provide any '
                'arguments: {}'.format(argv, line.strip()))

        wrapper = '{}/vectis-command-wrapper'.format(self.scratch)
        self.copy_to_guest(_WRAPPER, wrapper)
        self.check_call(['chmod', '+x', wrapper])
        self.command_wrapper = wrapper

        self.set_up_apt()

    def call(self, argv, **kwargs):
        assert self.call_argv is not None
        logger.info('%r: %r', self, argv)
        return subprocess.call(self.call_argv + list(argv), **kwargs)

    def check_call(self, argv, **kwargs):
        assert self.call_argv is not None
        logger.info('%r: %r', self, argv)
        subprocess.check_call(self.call_argv + list(argv), **kwargs)

    def check_output(self, argv, **kwargs):
        assert self.call_argv is not None
        logger.info('%r: %r', self, argv)
        return subprocess.check_output(self.call_argv + list(argv), **kwargs)

    def copy_to_guest(
        self,
        host_path: str,
        guest_path: str,
        *,
        cache=False
    ) -> None:
        assert host_path is not None
        assert guest_path is not None
        assert self.virt_process is not None
        stdin = self.virt_process.stdin
        stdout = self.virt_process.stdout
        assert stdin is not None
        assert stdout is not None

        if cache and self.__cached_copies.get(host_path) == guest_path:
            logger.info(
                'host:%s is already available at guest:%s, not copying again',
                host_path, guest_path,
            )
            return

        logger.info('Copying host:%s to guest:%s', host_path, guest_path)

        if not os.path.exists(host_path):
            raise WorkerError(
                'Cannot copy host:{!r} to guest: it does not '
                'exist'.format(host_path))

        if os.path.isdir(host_path):
            suffix = '/'
        else:
            suffix = ''

        stdin.write('copydown {}{} {}{}\n'.format(
            urllib.parse.quote(host_path),
            suffix,
            urllib.parse.quote(guest_path),
            suffix,
        ))
        stdin.flush()
        line = stdout.readline()

        if line != 'ok\n':
            raise WorkerError(
                'Failed to copy host:{!r} to guest:{!r}: {}'.format(
                    host_path, guest_path, line.strip()))

        if cache:
            self.__cached_copies[host_path] = guest_path

    def copy_to_host(
        self,
        guest_path: str,
        host_path: str,
    ) -> None:
        assert self.virt_process is not None
        stdin = self.virt_process.stdin
        stdout = self.virt_process.stdout
        assert stdin is not None
        assert stdout is not None

        if self.call(['test', '-e', guest_path]) != 0:
            raise WorkerError(
                'Cannot copy guest:{!r} to host: it does not exist'.format(
                    guest_path))

        logger.info('Copying guest:{} to host:{}'.format(
            guest_path, host_path))

        stdin.write('copyup {} {}\n'.format(
            urllib.parse.quote(guest_path),
            urllib.parse.quote(host_path),
        ))
        stdin.flush()
        line = stdout.readline()
        if line != 'ok\n':
            raise WorkerError(
                'Failed to copy guest:{!r} to host:{!r}: {}'.format(
                    guest_path, host_path, line.strip()))

    def open_shell(self) -> None:
        assert self.virt_process is not None
        stdin = self.virt_process.stdin
        stdout = self.virt_process.stdout
        assert stdin is not None
        assert stdout is not None

        stdin.write('shell\n')
        stdin.flush()
        line = stdout.readline()
        if line != 'ok\n':
            logger.warning('Unable to open a shell in guest: %s', line.strip())

    def set_up_apt(self) -> None:
        logger.info('Configuring apt in %r for %s', self, self.suite)

        with TemporaryDirectory(prefix='vectis-worker-') as tmp:
            sources_list = os.path.join(tmp, 'sources.list')

            with AtomicWriter(sources_list) as writer:
                self.write_sources_list(writer)

            self.copy_to_guest(sources_list, '/etc/apt/sources.list')

            apt_conf = os.path.join(tmp, 'apt.conf')

            with AtomicWriter(apt_conf) as writer:
                self.write_apt_conf(writer)

            self.copy_to_guest(apt_conf, '/etc/apt/apt.conf')
            self.check_call(['rm', '-f', '/etc/apt/apt/conf.d/01proxy'])

        self.install_apt_keys()

        if self.apt_update:
            self.check_call([
                'env', 'DEBIAN_FRONTEND=noninteractive',
            ] + self.archive_access.get_proxy_env() + [
                'apt-get', '-y', 'update',
            ])

    def install_apt_key(self, apt_key: str) -> None:
        self.copy_to_guest(
            apt_key,
            '/etc/apt/trusted.gpg.d/{}-{}'.format(
                uuid.uuid4(), os.path.basename(apt_key)))

    def make_file_available(
        self,
        filename: str,
        *,
        cache: bool = False,
        in_dir: Optional[str] = None,
        owner: Optional[str] = None
    ) -> str:
        if in_dir is None:
            in_dir = self.scratch

        if cache:
            in_guest = self.__cached_copies.get(filename)
            if (in_guest is not None and
                    os.path.commonpath([in_guest, in_dir]) == in_dir):
                return in_guest

        unique = str(uuid.uuid4())
        in_guest = '{}/{}/{}'.format(
            in_dir, unique, os.path.basename(filename))
        self.check_call(['mkdir', '{}/{}'.format(in_dir, unique)])
        self.copy_to_guest(filename, in_guest, cache=cache)

        if owner is not None:
            self.check_call([
                'chown', '-R', owner, '{}/{}'.format(in_dir, unique),
                in_guest,
            ])

        return in_guest

    def new_directory(
        self,
        prefix: str = '',
        tmpdir: Optional[str] = None,
    ) -> str:
        if not prefix:
            prefix = 'vectis-'

        if tmpdir is None:
            tmpdir = self.scratch

        d = self.check_output([
            'mktemp', '-d', '--tmpdir={}'.format(tmpdir),
            prefix + 'XXXXXXXXXX',
        ], universal_newlines=True).rstrip('\n')
        self.check_call(['chmod', '0755', d])
        return d

    def make_dsc_file_available(
        self,
        filename: str,
        owner: Optional[str] = None,
    ) -> Tuple[str, str]:
        d = os.path.dirname(filename) or os.curdir

        with open(filename) as reader:
            dsc = Dsc(reader)

        to = self.new_directory()
        files = [to, '{}/{}'.format(to, os.path.basename(filename))]
        self.copy_to_guest(filename, files[-1])

        for f in dsc['files']:
            files.append('{}/{}'.format(to, f['name']))
            self.copy_to_guest(os.path.join(d, f['name']), files[-1])

        if owner is not None:
            self.check_call(['chown', owner] + files)

        return files[0], os.path.basename(filename)

    def make_changes_file_available(
        self,
        filename: str,
        owner: Optional[str] = None,
    ) -> Tuple[str, str]:
        d = os.path.dirname(filename) or os.curdir

        with open(filename) as reader:
            changes = Changes(reader)

        to = self.new_directory()
        files = [to, '{}/{}'.format(to, os.path.basename(filename))]
        self.copy_to_guest(filename, files[-1])

        for f in changes['files']:
            files.append('{}/{}'.format(to, f['name']))
            self.copy_to_guest(os.path.join(d, f['name']), files[-1])

        if owner is not None:
            self.check_call(['chown', owner] + files)

        return files[0], os.path.basename(filename)

    def apt_install(
        self,
        packages: Iterable[str],
        *,
        may_fail: bool = False,
        recommends: bool = True
    ) -> None:
        argv = ['env', 'DEBIAN_FRONTEND=noninteractive']
        argv.extend(self.archive_access.get_proxy_env())
        argv.extend(['apt-get', '-y'])
        argv.append('-oDebug::pkgProblemResolver=true')

        if self.suite.needs_pinning:
            argv.extend(['-t', self.suite.apt_suite])

        if not recommends:
            argv.append('--no-install-recommends')

        argv.append('install')
        argv.extend(packages)

        if may_fail:
            self.call(argv)
        else:
            self.check_call(argv)
