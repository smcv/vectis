# Copyright © 2015-2017 Simon McVittie
# Copyright © 2017 Collabora Ltd.
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
import subprocess
import sys
from abc import abstractmethod, ABCMeta
from string import Template
from typing import (
    Any,
    Dict,
    Iterable,
    List,
    Mapping,
    Optional,
    Sequence,
    Set,
    TextIO,
    Tuple,
    Union,
    cast,
)
from urllib.parse import urlparse
from weakref import WeakValueDictionary

from vectis.error import Error
from vectis.util import parse_byte_size

import yaml


class ConfigError(Error):
    pass


logger = logging.getLogger(__name__)

HOME = os.path.expanduser('~')
XDG_CACHE_HOME = os.getenv('XDG_CACHE_HOME', os.path.expanduser('~/.cache'))
XDG_CONFIG_HOME = os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config'))
XDG_CONFIG_DIRS = os.getenv('XDG_CONFIG_DIRS', '/etc/xdg')
XDG_DATA_HOME = os.getenv(
    'XDG_DATA_HOME', os.path.expanduser('~/.local/share'))
XDG_DATA_DIRS = os.getenv(
    'XDG_DATA_DIRS', os.path.expanduser('~/.local/share'))

_1M = 1024 * 1024


class ArchiveAccess:

    def __init__(
        self,
        *,
        mirrors: Optional[Mapping[Optional[str], str]] = None,
        http_proxy: Optional[str] = None,
        https_proxy: Optional[str] = None,
        ftp_proxy: Optional[str] = None,
        no_proxy: Sequence[str] = (),
    ) -> None:
        if mirrors is None:
            self._raw_mirrors: Mapping[Optional[str], str] = {}
        else:
            self._raw_mirrors = mirrors

        self._proxies = {
            'ftp': ftp_proxy,
            'http': http_proxy,
            'https': https_proxy,
        }
        self._no_proxy = no_proxy

    def _lookup_mirror(self, suite: 'Suite') -> Optional[str]:
        for uri in suite.uris:
            value = self._raw_mirrors.get(uri)

            if value is not None:
                return value

            value = self._raw_mirrors.get(uri.rstrip('/'))

            if value is not None:
                return value

        value = self._raw_mirrors.get(str(suite.archive))

        if value is not None:
            return value

        value = self._raw_mirrors.get(None)

        if value is not None:
            return value

        return None

    def _lookup_proxy(self, uri: Optional[str] = None) -> Optional[str]:
        if uri is not None:
            parsed = urlparse(uri)

            if parsed.hostname is None:
                return 'DIRECT'

            for nope in self._no_proxy:
                if parsed.hostname == nope:
                    return 'DIRECT'
                elif nope.startswith('.') and parsed.hostname.endswith(nope):
                    return 'DIRECT'

            return self._proxies.get(parsed.scheme)

        # guess that http is the most general-purpose
        return self._proxies.get('http')

    def apt_proxy_for_suite(self, suite: 'Suite') -> str:
        value = self._lookup_proxy(self.mirror_for_suite(suite))

        if value is None:
            value = 'DIRECT'

        return value

    def mirror_for_suite(self, suite: 'Suite') -> str:
        v = self._lookup_mirror(suite)

        if v is None:
            if suite.uris:
                t = suite.uris[0]
            else:
                raise ConfigError('No mirror configuration for %s' % suite)
        else:
            t = v

        return Template(t).substitute(
            archive=suite.archive,
        )

    def check_suite(self, suite: 'Suite') -> None:
        m = self._lookup_mirror(suite)
        p = self._lookup_proxy(self.mirror_for_suite(suite))

        if m is None and p is None:
            raise ConfigError(
                'No mirror or proxy configuration for %s' % suite)

    def get_proxy_env(self) -> List[str]:
        unset = []
        values = []

        for k, v in self._proxies.items():
            if v in (None, 'DIRECT'):
                unset.append(k + '_proxy')
            else:
                values.append('{}_proxy={}'.format(k, v))

        if self._no_proxy:
            values.append('no_proxy={}'.format(','.join(self._no_proxy)))
        else:
            unset.append('no_proxy')

        result = ['env']

        for u in unset:
            result.append('-u')
            result.append(u)

        return result + values

    @property
    def ftp_proxy(self) -> str:
        return self._proxies.get('ftp') or 'DIRECT'

    @property
    def http_proxy(self) -> str:
        return self._proxies.get('http') or 'DIRECT'

    @property
    def https_proxy(self) -> str:
        return self._proxies.get('https') or 'DIRECT'

    @property
    def no_proxy(self) -> str:
        return ','.join(self._no_proxy)

    def write_apt_conf(
        self,
        writer: TextIO,
        hosts: Iterable[str] = (),
    ) -> None:

        for k, v in self._proxies.items():
            logger.info(
                'Acquire::%s::Proxy "%s";', k, v or 'DIRECT')
            writer.write(
                'Acquire::{}::Proxy "{}";\n'.format(k, v or 'DIRECT'))

        for nope in self._no_proxy:
            if nope.startswith('.'):
                for host in hosts:
                    if host.endswith(nope):
                        logger.info(
                            'Acquire::*::Proxy::%s "DIRECT";', host)
                        writer.write(
                            'Acquire::ftp::Proxy::{} "DIRECT";\n'.format(
                                host))
                        writer.write(
                            'Acquire::http::Proxy::{} "DIRECT";\n'.format(
                                host))
                        writer.write(
                            'Acquire::https::Proxy::{} "DIRECT";\n'.format(
                                host))
            else:
                logger.info(
                    'Acquire::*::Proxy::%s "DIRECT";', nope)
                writer.write(
                    'Acquire::ftp::Proxy::{} "DIRECT";\n'.format(nope))
                writer.write(
                    'Acquire::http::Proxy::{} "DIRECT";\n'.format(nope))
                writer.write(
                    'Acquire::https::Proxy::{} "DIRECT";\n'.format(nope))


class _ConfigLike(metaclass=ABCMeta):

    def __init__(self) -> None:
        pass

    def _get_string_set(self, name: str) -> Set[str]:
        value = self._get(name)

        if value is None:
            return set()
        elif isinstance(value, str):
            return set(value.split())
        else:
            return set(value)

    def _get_int(self, name: str) -> int:
        return int(self._get(name))

    @property
    def all_components(self) -> Set[str]:
        return self.components | self.extra_components

    @property
    def components(self) -> Set[str]:
        return self._get_string_set('components')

    @property
    def extra_components(self) -> Set[str]:
        return self._get_string_set('extra_components')

    @abstractmethod
    def _get(
        self,
        name: str,
    ) -> Any:
        raise NotImplementedError


class Vendor(_ConfigLike):

    def __init__(self, name: str, raw: Sequence[Dict[str, Any]]) -> None:
        super(Vendor, self).__init__()
        self._name = name
        self._raw = raw

        for r in self._raw:
            p = r.get('vendors', {}).get(self._name, {})
            suites = p.get('suites', {})

            for suite in suites.keys():
                if '*' not in suite:
                    continue

                if (not suite.startswith('*-') or
                        '*' in suite[2:]):
                    raise ConfigError(
                        'Suite wildcards must be of the form *-something')

    @property
    def default_suite(self) -> str:
        return str(self._get('default_suite'))

    @property
    def default_worker_suite(self) -> str:
        return str(self._get('default_worker_suite'))

    def __str__(self) -> str:
        return self._name

    def __repr__(self) -> str:
        return '<Vendor {!r}>'.format(self._name)

    def _get(self, name: str) -> Any:
        if name not in self._raw[-1]['defaults']:
            raise KeyError('{!r} does not configure {!r}'.format(self, name))

        for r in self._raw:
            p = r.get('vendors', {}).get(self._name, {})

            if name in p:
                return p[name]

        for r in self._raw:
            d = r.get('defaults', {})

            if name in d:
                return d[name]

        # We already checked that it was in _raw[-1], which is the set of
        # hard-coded defaults from this file, as augmented with environment
        # variables etc.
        raise AssertionError('Not reached')


class Suite(_ConfigLike):

    def __init__(
        self,
        name: str,
        vendor: Vendor,
        raw: Sequence[Dict[str, Any]],
        base: 'Optional[Suite]' = None,
        pattern: Optional[str] = None
    ) -> None:
        super(Suite, self).__init__()
        self._name = name
        self._vendor = vendor
        self._raw = raw
        self.base = base

        if pattern is None:
            self._pattern = name
        else:
            self._pattern = pattern

        self.base = base
        self.hierarchy: List[Suite] = []
        suite: Optional[Suite] = self

        while suite is not None:
            self.hierarchy.append(suite)
            suite = suite.base

    @property
    def vendor(self) -> Vendor:
        return self._vendor

    @property
    def suite(self) -> 'Suite':
        return self

    @property
    def merged_usr_allowed(self) -> bool:
        return bool(self._get('merged_usr_allowed'))

    @property
    def merged_usr_required(self) -> bool:
        return bool(self._get('merged_usr_required'))

    @property
    def needs_pinning(self) -> bool:
        return bool(
            self._get(
                'needs_pinning',
                inherit_from_ancestors=False,
                inherit_from_defaults=False,
            )
        )

    @property
    def sbuild_resolver(self) -> Sequence[str]:
        return cast(Sequence[str], self._get('sbuild_resolver'))

    @property
    def cross_sbuild_resolver(self) -> Sequence[str]:
        return cast(
            Sequence[str],
            self._get('cross_sbuild_resolver')
        ) or self.sbuild_resolver

    @property
    def apt_key(self) -> Optional[str]:
        value = self._get('apt_key')

        if value is None:
            return None

        svalue = str(value)

        if '/' in svalue:
            return svalue

        return os.path.join(os.path.dirname(__file__), 'keys', svalue)

    def __str__(self) -> str:
        return self._name

    def __repr__(self) -> str:
        return '<Suite {!r}/{!r}>'.format(self._vendor, self._name)

    def _get(
        self,
        name: str,
        inherit_from_ancestors: bool = True,
        inherit_from_defaults: bool = True,
        inherit_from_vendor: bool = True,
    ) -> Any:
        if (
            name not in self._raw[-1]['defaults']
            and name not in (
                'apt_suite',
                'apt_trusted',
                'archive',
                'base',
                'needs_pinning',
            )
        ):
            raise KeyError('{!r} does not configure {!r}'.format(self, name))

        for r in self._raw:
            p = r.get('vendors', {}).get(str(self._vendor), {})
            s = p.get('suites', {}).get(self._pattern, {})

            if name in s:
                return s[name]

        if inherit_from_ancestors:
            for ancestor in self.hierarchy[1:]:
                value = ancestor._get(
                    name,
                    inherit_from_ancestors=False,
                    inherit_from_defaults=False,
                    inherit_from_vendor=False,
                )

                if value is not None:
                    return value

        if inherit_from_vendor:
            for r in self._raw:
                p = r.get('vendors', {}).get(str(self._vendor), {})

                if name in p:
                    return p[name]

        if inherit_from_defaults:
            for r in self._raw:
                d = r.get('defaults', {})

                if name in d:
                    return d[name]

        return None

    @property
    def apt_suite(self) -> str:
        suite = self._get(
            'apt_suite',
            inherit_from_ancestors=False,
            inherit_from_defaults=False,
            inherit_from_vendor=False,
        )

        if suite is None:
            return str(self.suite)

        assert isinstance(suite, str)

        if '*' in suite and self.base is not None:
            return suite.replace('*', str(self.base))

        return suite

    @property
    def apt_trusted(self) -> bool:
        return bool(
            self._get(
                'apt_trusted',
                inherit_from_ancestors=False,
                inherit_from_defaults=False,
                inherit_from_vendor=False,
            )
        )

    @property
    def archive(self) -> str:
        value = self._get(
            'archive',
            inherit_from_ancestors=False,
            inherit_from_defaults=False,
        )

        if value is None:
            value = str(self.vendor)

        assert isinstance(value, str)

        return value

    @property
    def vmdebootstrap_options(self) -> Sequence[str]:
        return cast(Sequence[str], self._get('vmdebootstrap_options'))

    @property
    def uris(self) -> Sequence[str]:
        return cast(
            Sequence[str],
            self._get('uris') or self.vendor._get('uris')
        )


class Directory(_ConfigLike):

    def __init__(
        self,
        path: str,
        raw: Sequence[Dict[str, Any]],
    ) -> None:
        super(Directory, self).__init__()
        self._path = path
        self._raw = raw

    def __str__(self) -> str:
        return self._path

    def __repr__(self) -> str:
        return '<Directory {!r}>'.format(self._path)

    def _get(self, name: str) -> Any:
        if name not in self._raw[-1]['defaults']:
            raise KeyError('{!r} does not configure {!r}'.format(self, name))

        for r in self._raw:
            d = r.get('directories', {}).get(self._path, {})

            if name in d:
                return d[name]

        raise KeyError(name)


class ConfigView(_ConfigLike, metaclass=ABCMeta):
    def __init__(self) -> None:
        super(ConfigView, self).__init__()

    @abstractmethod
    def _get(
        self,
        name: str,
        inherit_from_suite: bool = True,
        inherit_from_vendor: bool = True,
    ) -> Any:
        raise NotImplementedError

    @abstractmethod
    def can_override(self, name: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def get_vendor(self, name: str) -> Vendor:
        raise NotImplementedError

    def _get_filenames(
        self,
        name: str,
        default: Sequence[str] = ()
    ) -> Sequence[str]:
        value = cast(Union[None, str, Sequence[str]], self._get(name))

        if value is None:
            value = default

        if isinstance(value, str):
            values: Iterable[str] = [value]
        else:
            values = value

        values = map(os.path.expandvars, values)
        values = map(os.path.expanduser, values)
        return list(values)

    def _get_filename(
        self,
        name: str,
        default: Optional[str] = None,
    ) -> Optional[str]:
        value = self._get_str(name)

        if value is None:
            value = default

        if value is None:
            return value

        assert isinstance(value, str)
        value = os.path.expandvars(value)
        value = os.path.expanduser(value)
        return value

    def _get_str(
        self,
        name: str,
        default: Optional[str] = None
    ) -> Optional[str]:
        value = self._get(name)

        if value is None:
            value = default

        assert isinstance(value, str) or value is None
        return value

    @property
    def parallel(self) -> int:
        return self._get_int('parallel')

    @property
    def build_indep_together(self) -> bool:
        return self._get_bool('build_indep_together')

    @property
    def sbuild_source_together(self) -> bool:
        return self._get_bool('sbuild_source_together')

    def _get_bool(self, name: str) -> bool:
        value = self._get(name)

        if isinstance(value, bool):
            return value

        raise ConfigError(
            'Invalid value for {!r}: {!r} is not a boolean value'.format(
                name, value))

    def _get_mandatory_string(self, name: str) -> str:
        value = self._get(name)

        if isinstance(value, str):
            return value

        raise ConfigError(
            '{!r} key {!r} has no default and must be configured'.format(
                self, name))

    @property
    def storage(self) -> str:
        ret = self._get_filename(
            'storage', os.path.join(XDG_CACHE_HOME, 'vectis'))
        assert ret is not None
        return ret

    @property
    def qemu_overlay_dir(self) -> Optional[str]:
        return self._get_str('qemu_overlay_dir')

    @property
    def qemu_ram_size(self) -> int:
        return parse_byte_size(self._get('qemu_ram_size'))

    @property
    def output_dir(self) -> Optional[str]:
        """
        The directory in which we will place the results, overriding
        output_parent.
        """
        return self._get_filename('output_dir')

    @property
    def output_parent(self) -> Optional[str]:
        """
        The directory in which we will create a new subdirectory for the
        results.
        """
        return self._get_filename('output_parent')

    @property
    def link_builds(self) -> Sequence[str]:
        return self._get_filenames('link_builds', ())

    @property
    def worker_architecture(self) -> str:
        value = self._get('worker_architecture')

        if value is None:
            value = self.architecture

        return value

    @property
    def lxc_worker_architecture(self) -> str:
        value = self._get('lxc_worker_architecture')

        if value is None:
            value = self.worker_architecture

        return value

    @property
    def lxd_worker_architecture(self) -> str:
        value = self._get('lxd_worker_architecture')

        if value is None:
            value = self.worker_architecture

        return value

    @property
    def pbuilder_worker_architecture(self) -> str:
        value = self._get('pbuilder_worker_architecture')

        if value is None:
            value = self.worker_architecture

        return value

    @property
    def piuparts_worker_architecture(self) -> str:
        value = self._get('piuparts_worker_architecture')

        if value is None:
            value = self.worker_architecture

        return value

    @property
    def sbuild_worker_architecture(self) -> str:
        value = self._get('sbuild_worker_architecture')

        if value is None:
            value = self.worker_architecture

        return value

    @property
    def vmdb2_worker_architecture(self) -> str:
        value = self._get('vmdb2_worker_architecture')

        if value is None:
            value = self.worker_architecture

        return value

    @property
    def vmdebootstrap_worker_architecture(self) -> str:
        value = self._get('vmdebootstrap_worker_architecture')

        if value is None:
            value = self.worker_architecture

        return value

    def get_kernel_package(self, architecture: str) -> Optional[str]:
        mapping = self._get('kernel_package')

        if not isinstance(mapping, dict):
            mapping = {None: mapping}

        value = mapping.get(architecture)

        if value is None:
            value = mapping.get(None)

        return value

    @property
    def suite(self) -> Optional[Suite]:
        suite = self._get('suite', inherit_from_suite=False)

        if suite is None:
            return None

        assert isinstance(suite, str), (repr(suite), repr(self))
        return self.ensure_suite(self.vendor, suite)

    @property
    def vendor(self) -> Vendor:
        return self.get_vendor(
            self._get(
                'vendor',
                inherit_from_suite=False,
                inherit_from_vendor=False,
            )
        )

    @property
    def worker_vendor(self) -> Vendor:
        return self.get_vendor(self._get('worker_vendor'))

    @property
    def vmdb2_worker_vendor(self) -> Vendor:
        value = self._get('vmdb2_worker_vendor')

        if value is None:
            value = self._get('worker_vendor')

        return self.get_vendor(value)

    @property
    def vmdebootstrap_worker_vendor(self) -> Vendor:
        value = self._get('vmdebootstrap_worker_vendor')

        if value is None:
            value = self._get('worker_vendor')

        return self.get_vendor(value)

    @property
    def lxc_worker_vendor(self) -> Vendor:
        value = self._get('lxc_worker_vendor')

        if value is None:
            value = self._get('worker_vendor')

        return self.get_vendor(value)

    @property
    def lxd_worker_vendor(self) -> Vendor:
        value = self._get('lxd_worker_vendor')

        if value is None:
            value = self._get('worker_vendor')

        return self.get_vendor(value)

    @property
    def pbuilder_worker_vendor(self) -> Vendor:
        value = self._get('pbuilder_worker_vendor')

        if value is None:
            value = self._get('worker_vendor')

        return self.get_vendor(value)

    @property
    def piuparts_worker_vendor(self) -> Vendor:
        value = self._get('piuparts_worker_vendor')

        if value is None:
            value = self._get('worker_vendor')

        return self.get_vendor(value)

    @property
    def sbuild_worker_vendor(self) -> Vendor:
        value = self._get('sbuild_worker_vendor')

        if value is None:
            value = self._get('worker_vendor')

        return self.get_vendor(value)

    @property
    def worker_suite(self) -> Optional[Suite]:
        value = self._get('worker_suite')

        if value is None:
            value = self.worker_vendor.default_worker_suite

        if value is None:
            return None

        return self.ensure_suite(self.worker_vendor, value)

    @property
    def lxc_worker_suite(self) -> Optional[Suite]:
        value = self._get('lxc_worker_suite')

        if value is None:
            value = self.lxc_worker_vendor.default_worker_suite

        if value is None:
            return None

        return self.ensure_suite(self.lxc_worker_vendor, value)

    @property
    def lxd_worker_suite(self) -> Optional[Suite]:
        value = self._get('lxd_worker_suite')

        if value is None:
            value = self.lxd_worker_vendor.default_worker_suite

        if value is None:
            return None

        return self.ensure_suite(self.lxd_worker_vendor, value)

    @property
    def pbuilder_worker_suite(self) -> Optional[Suite]:
        value = self._get('pbuilder_worker_suite')

        if value is None:
            value = self.pbuilder_worker_vendor.default_worker_suite

        if value is None:
            return None

        return self.ensure_suite(self.pbuilder_worker_vendor, value)

    @property
    def piuparts_worker_suite(self) -> Optional[Suite]:
        value = self._get('piuparts_worker_suite')

        if value is None:
            value = self.piuparts_worker_vendor.default_worker_suite

        if value is None:
            return None

        return self.ensure_suite(self.piuparts_worker_vendor, value)

    @property
    def sbuild_worker_suite(self) -> Optional[Suite]:
        value = self._get('sbuild_worker_suite')

        if value is None:
            value = self.sbuild_worker_vendor.default_worker_suite

        if value is None:
            return None

        return self.ensure_suite(self.sbuild_worker_vendor, value)

    @property
    def vmdb2_worker_suite(self) -> Optional[Suite]:
        value = self._get('vmdb2_worker_suite')

        if value is None:
            value = self.vmdb2_worker_vendor.default_worker_suite

        if value is None:
            return None

        return self.ensure_suite(self.vmdb2_worker_vendor, value)

    @property
    def vmdebootstrap_worker_suite(self) -> Optional[Suite]:
        value = self._get('vmdebootstrap_worker_suite')

        if value is None:
            value = self.vmdebootstrap_worker_vendor.default_worker_suite

        if value is None:
            return None

        return self.ensure_suite(self.vmdebootstrap_worker_vendor, value)

    @property
    def piuparts_ignore(self) -> Sequence[str]:
        return self._get('piuparts_ignore')

    @property
    def piuparts_ignore_regexes(self) -> Sequence[str]:
        return self._get('piuparts_ignore_regexes')

    @property
    def piuparts_tarballs(self) -> Sequence[str]:
        return self._get('piuparts_tarballs')

    @property
    def qemu_image(self) -> str:
        value = self._get('qemu_image')

        assert value is not None
        assert isinstance(value, str)
        suite = self.suite

        if suite is None:
            raise ConfigError('suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.vendor),
                str(suite.hierarchy[-1]),
                self.architecture,
                value,
            )

        return value

    @property
    def worker_qemu_image(self) -> str:
        value = self._get('worker_qemu_image')

        if value is None:
            value = self.worker_vendor._get('qemu_image')

        assert value is not None
        suite = self.worker_suite

        if suite is None:
            raise ConfigError('worker_suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.worker_vendor),
                str(suite.hierarchy[-1]),
                self.worker_architecture,
                value,
            )

        return value

    @property
    def lxc_worker_qemu_image(self) -> str:
        value = self._get('lxc_worker_qemu_image')

        if value is None:
            value = self.lxc_worker_vendor._get('qemu_image')

        assert value is not None
        suite = self.lxc_worker_suite

        if suite is None:
            raise ConfigError('lxc_worker_suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.lxc_worker_vendor),
                str(suite.hierarchy[-1]),
                self.lxc_worker_architecture,
                value,
            )

        return value

    @property
    def lxd_worker_qemu_image(self) -> str:
        value = self._get('lxd_worker_qemu_image')

        if value is None:
            value = self.lxd_worker_vendor._get('qemu_image')

        assert value is not None
        suite = self.lxd_worker_suite

        if suite is None:
            raise ConfigError('lxd_worker_suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.lxd_worker_vendor),
                str(suite.hierarchy[-1]),
                self.lxd_worker_architecture,
                value,
            )

        return value

    @property
    def pbuilder_worker_qemu_image(self) -> str:
        value = self._get('pbuilder_worker_qemu_image')

        if value is None:
            value = self.pbuilder_worker_vendor._get('qemu_image')

        assert value is not None
        suite = self.pbuilder_worker_suite

        if suite is None:
            raise ConfigError('pbuilder_worker_suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.pbuilder_worker_vendor),
                str(suite.hierarchy[-1]),
                self.pbuilder_worker_architecture,
                value,
            )

        return value

    @property
    def piuparts_worker_qemu_image(self) -> str:
        value = self._get('piuparts_worker_qemu_image')

        if value is None:
            value = self.piuparts_worker_vendor._get('qemu_image')

        assert value is not None
        suite = self.piuparts_worker_suite

        if suite is None:
            raise ConfigError('piuparts_worker_suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.piuparts_worker_vendor),
                str(suite.hierarchy[-1]),
                self.piuparts_worker_architecture,
                value,
            )

        return value

    @property
    def sbuild_worker_qemu_image(self) -> str:
        value = self._get('sbuild_worker_qemu_image')

        if value is None:
            value = self.sbuild_worker_vendor._get('qemu_image')

        assert value is not None
        suite = self.sbuild_worker_suite

        if suite is None:
            raise ConfigError('sbuild_worker_suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.sbuild_worker_vendor),
                str(suite.hierarchy[-1]),
                self.sbuild_worker_architecture,
                value,
            )

        return value

    @property
    def write_qemu_image(self) -> str:
        return self._get('write_qemu_image')

    @property
    def worker(self) -> Sequence[str]:
        value = self._get('worker')

        if value is None:
            value = ['qemu']

            if self.qemu_overlay_dir is not None:
                value.append('--overlay-dir={}'.format(self.qemu_overlay_dir))

            if self.qemu_ram_size is not None:
                value.append('--ram-size={}'.format(self.qemu_ram_size // _1M))

            value.append('--cpus={}'.format(self.parallel))
            value.append(self.worker_qemu_image)

        return value

    @property
    def lxc_worker(self) -> Sequence[str]:
        value = self._get('lxc_worker')

        if value is None:
            value = ['qemu']

            if self.qemu_overlay_dir is not None:
                value.append('--overlay-dir={}'.format(self.qemu_overlay_dir))

            if self.qemu_ram_size is not None:
                value.append('--ram-size={}'.format(self.qemu_ram_size // _1M))

            value.append('--cpus={}'.format(self.parallel))
            value.append(self.lxc_worker_qemu_image)

        return value

    @property
    def lxd_worker(self) -> Sequence[str]:
        value = self._get('lxd_worker')

        if value is None:
            value = ['qemu']

            if self.qemu_overlay_dir is not None:
                value.append('--overlay-dir={}'.format(self.qemu_overlay_dir))

            if self.qemu_ram_size is not None:
                value.append('--ram-size={}'.format(self.qemu_ram_size // _1M))

            value.append('--cpus={}'.format(self.parallel))
            value.append(self.lxd_worker_qemu_image)

        return value

    @property
    def pbuilder_worker(self) -> Sequence[str]:
        value = self._get('pbuilder_worker')

        if value is None:
            value = ['qemu']

            if self.qemu_overlay_dir is not None:
                value.append('--overlay-dir={}'.format(self.qemu_overlay_dir))

            if self.qemu_ram_size is not None:
                value.append('--ram-size={}'.format(self.qemu_ram_size // _1M))

            value.append('--cpus={}'.format(self.parallel))
            value.append(self.pbuilder_worker_qemu_image)

        return value

    @property
    def piuparts_worker(self) -> Sequence[str]:
        value = self._get('piuparts_worker')

        if value is None:
            value = ['qemu']

            if self.qemu_overlay_dir is not None:
                value.append('--overlay-dir={}'.format(self.qemu_overlay_dir))

            if self.qemu_ram_size is not None:
                value.append('--ram-size={}'.format(self.qemu_ram_size // _1M))

            value.append('--cpus={}'.format(self.parallel))
            value.append(self.piuparts_worker_qemu_image)

        return value

    @property
    def sbuild_worker(self) -> Sequence[str]:
        value = self._get('sbuild_worker')

        if value is None:
            value = ['qemu']

            if self.qemu_overlay_dir is not None:
                value.append('--overlay-dir={}'.format(self.qemu_overlay_dir))

            if self.qemu_ram_size is not None:
                value.append('--ram-size={}'.format(self.qemu_ram_size // _1M))

            value.append('--cpus={}'.format(self.parallel))
            value.append(self.sbuild_worker_qemu_image)

        return value

    @property
    def vmdb2_worker_qemu_image(self) -> str:
        value = self._get('vmdb2_worker_qemu_image')

        if value is None:
            value = self.vmdb2_worker_vendor._get('qemu_image')

        assert value is not None
        suite = self.vmdb2_worker_suite

        if suite is None:
            raise ConfigError('vmdb2_worker_suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.vmdb2_worker_vendor),
                str(suite.hierarchy[-1]),
                self.vmdb2_worker_architecture,
                value,
            )

        return value

    @property
    def vmdb2_worker(self) -> Sequence[str]:
        value = self._get('vmdb2_worker')

        if value is None:
            value = ['qemu']

            if self.qemu_overlay_dir is not None:
                value.append('--overlay-dir={}'.format(self.qemu_overlay_dir))

            if self.qemu_ram_size is not None:
                value.append('--ram-size={}'.format(self.qemu_ram_size // _1M))

            value.append('--cpus={}'.format(self.parallel))
            value.append(self.vmdb2_worker_qemu_image)

        return value

    @property
    def vmdebootstrap_worker_qemu_image(self) -> str:
        value = self._get('vmdebootstrap_worker_qemu_image')

        if value is None:
            value = self.vmdebootstrap_worker_vendor._get('qemu_image')

        assert value is not None
        suite = self.vmdebootstrap_worker_suite

        if suite is None:
            raise ConfigError('vmdebootstrap_worker_suite not defined')

        if '/' not in value:
            return os.path.join(
                self.storage,
                str(self.vmdebootstrap_worker_vendor),
                str(suite.hierarchy[-1]),
                self.vmdebootstrap_worker_architecture,
                value,
            )

        return value

    @property
    def vmdebootstrap_worker(self) -> Sequence[str]:
        value = self._get('vmdebootstrap_worker')

        if value is None:
            value = ['qemu']

            if self.qemu_overlay_dir is not None:
                value.append('--overlay-dir={}'.format(self.qemu_overlay_dir))

            if self.qemu_ram_size is not None:
                value.append('--ram-size={}'.format(self.qemu_ram_size // _1M))

            value.append('--cpus={}'.format(self.parallel))
            value.append(self.vmdebootstrap_worker_qemu_image)

        return value

    @property
    def debootstrap_script(self) -> Optional[str]:
        value = self._get('debootstrap_script')

        if value is not None:
            return value

        if self.suite is None:
            return None

        return str(self.suite)

    @property
    def apt_key(self) -> Optional[str]:
        value = self._get('apt_key')

        if value is None:
            return None

        if '/' in value:
            return value

        return os.path.join(os.path.dirname(__file__), 'keys', value)

    def get_archive_access(self) -> ArchiveAccess:
        return ArchiveAccess(
            ftp_proxy=self._get('ftp_proxy'),
            http_proxy=self._get('http_proxy'),
            https_proxy=self._get('https_proxy'),
            mirrors=self._get('mirrors'),
            no_proxy=self._get('no_proxy'),
        )

    @property
    def no_proxy(self) -> Sequence[str]:
        return self._get('no_proxy')

    @abstractmethod
    def ensure_suite(
        self,
        vendor: Vendor,
        name: str,
    ) -> Suite:
        raise NotImplementedError

    @abstractmethod
    def get_existing_suite(
        self,
        vendor: Vendor,
        name: str,
    ) -> Optional[Suite]:
        raise NotImplementedError

    @property
    def architecture(self) -> str:
        return self._get('architecture')

    @property
    def lxc_24bit_subnet(self) -> str:
        return self._get('lxc_24bit_subnet')

    @property
    def kernel_package(self) -> Optional[Mapping[str, str]]:
        return self._get('kernel_package')

    @property
    def default_suite(self) -> str:
        return self._get('default_suite')

    @property
    def default_worker_suite(self) -> str:
        return self._get('default_worker_suite')

    @property
    def qemu_image_size(self) -> int:
        return parse_byte_size(self._get('qemu_image_size'))

    @property
    def sbuild_resolver(self) -> Sequence[str]:
        return cast(Sequence[str], self._get('sbuild_resolver'))

    @property
    def cross_sbuild_resolver(self) -> Sequence[str]:
        return cast(
            Sequence[str],
            self._get('cross_sbuild_resolver')
        ) or self.sbuild_resolver

    @property
    def autopkgtest(self) -> Sequence[str]:
        return self._get('autopkgtest')

    @property
    def orig_dirs(self) -> Sequence[str]:
        return self._get('orig_dirs')

    @property
    def dpkg_source_tar_ignore(self) -> Sequence[str]:
        return self._get('dpkg_source_tar_ignore')

    @property
    def dpkg_source_diff_ignore(self) -> Optional[str]:
        return self._get('dpkg_source_diff_ignore')

    @property
    def dpkg_source_extend_diff_ignore(self) -> Sequence[str]:
        return self._get('dpkg_source_extend_diff_ignore')

    @property
    def apt_key_package(self) -> str:
        return self._get('apt_key_package')


class Config(ConfigView):

    def __init__(
        self,
        config_layers: Sequence[Dict[str, Any]] = (),
        current_directory: Optional[str] = None
    ):
        super(Config, self).__init__()
        self._relevant_directory = None
        self._suites: WeakValueDictionary[Tuple[str, str], Suite] = (
            WeakValueDictionary()
        )
        self._vendors: Dict[str, Vendor] = {}

        d = yaml.safe_load(
            open(os.path.join(os.path.dirname(__file__), 'defaults.yaml')))

        # Some things can have better defaults that can't be hard-coded
        d['defaults']['parallel'] = str(os.cpu_count())

        try:
            d['defaults']['architecture'] = subprocess.check_output(
                ['dpkg', '--print-architecture'],
                universal_newlines=True).strip()
        except subprocess.CalledProcessError:
            pass

        d['vendors']['debian']['default_suite'] = 'sid'

        try:
            import distro_info
        except ImportError:
            d['vendors']['debian']['default_worker_suite'] = 'sid'
        else:
            debian = distro_info.DebianDistroInfo()
            ubuntu = distro_info.UbuntuDistroInfo()
            d['vendors']['debian']['default_worker_suite'] = debian.stable()
            d['vendors']['debian']['suites']['stable'] = {
                'alias_for': debian.stable(),
            }
            d['vendors']['debian']['suites']['stable-security'] = {
                'alias_for': debian.stable() + '-security',
            }
            d['vendors']['debian']['suites']['testing'] = {
                'alias_for': debian.testing(),
            }
            d['vendors']['debian']['suites']['oldstable'] = {
                'alias_for': debian.old(),
            }
            d['vendors']['debian']['suites']['oldstable-security'] = {
                'alias_for': debian.old() + '-security',
            }
            d['vendors']['debian']['refresh_suites'] = [
                'sid',
                'testing',
                'stable',
            ]

            # According to autopkgtest-buildvm-ubuntu-cloud, just after
            # an Ubuntu release there is briefly no development version
            # at all.
            try:
                ubuntu_devel = ubuntu.devel()
            except distro_info.DistroDataOutdated:
                ubuntu_devel = ubuntu.stable()

            d['vendors']['ubuntu']['default_suite'] = ubuntu_devel
            d['vendors']['ubuntu']['default_worker_suite'] = ubuntu.lts()
            d['vendors']['ubuntu']['suites']['devel'] = {
                'alias_for': ubuntu_devel,
            }

            for suite in debian.all:
                d['vendors']['debian']['suites'].setdefault(suite, {})

            for suite in ubuntu.all:
                d['vendors']['ubuntu']['suites'].setdefault(suite, {})

            d['vendors']['ubuntu']['refresh_suites'] = list(set([
                ubuntu_devel,
                ubuntu.stable(),
                ubuntu.lts(),
            ]))

        self._raw = []
        self._raw.append(d)

        if config_layers:
            self._raw[:0] = list(config_layers)
        else:
            config_dirs = XDG_CONFIG_DIRS.split(':')
            config_dirs = list(reversed(config_dirs))
            config_dirs.append(XDG_CONFIG_HOME)
            for p in config_dirs:
                conffile = os.path.join(p, 'vectis', 'vectis.yaml')

                try:
                    reader = open(conffile)
                except FileNotFoundError:
                    continue

                with reader:
                    raw = yaml.safe_load(reader)

                    if not isinstance(raw, dict):
                        raise ConfigError(
                            'Reading {!r} did not yield a dict'.format(
                                conffile))

                    self._raw.insert(0, raw)

        if current_directory is None:
            current_directory = os.getcwd()

        self._relevant_directory = None

        while self._relevant_directory is None:
            for r in self._raw:
                if current_directory in r.get('directories', {}):
                    self._relevant_directory = current_directory
                    break
            else:
                parent, _ = os.path.split(current_directory)
                # Guard against infinite recursion. If current_directory == '/'
                # we would already have found directories./ in the hard-coded
                # defaults, and broken out of the loop
                assert len(parent) < len(current_directory)
                current_directory = parent
                continue

        assert self._relevant_directory is not None

        self.path_based: Directory = Directory(
            self._relevant_directory, self._raw
        )

    def can_override(self, name: str) -> bool:
        return name in self._raw[-1]['defaults']

    def dump(self, stream: TextIO = sys.stdout) -> None:
        d = {}

        for k in sorted(self._raw[-1]['defaults']):
            try:
                v = getattr(self, k)
            except Exception as e:
                print('# {}: {}'.format(k, repr(str(e))), file=stream)
            else:
                if isinstance(v, (set, tuple)):
                    v = list(v)
                elif isinstance(v, Suite) or isinstance(v, Vendor):
                    v = str(v)

                d[k] = v

        yaml.dump(d, stream)

    def _get(
        self,
        name: str,
        inherit_from_suite: bool = True,
        inherit_from_vendor: bool = True,
    ) -> Any:
        if name not in self._raw[-1]['defaults']:
            raise KeyError('{!r} does not configure {!r}'.format(self, name))

        try:
            return self.path_based._get(name)
        except KeyError:
            pass

        if (
            inherit_from_suite
            and self.suite is not None
        ):
            return self.suite._get(name)

        if inherit_from_vendor:
            return self.vendor._get(name)

        for r in self._raw:
            if name in r.get('defaults', {}):
                return r['defaults'][name]

        return None

    def get_vendor(self, name: str) -> Vendor:
        assert isinstance(name, str), repr(name)
        if name not in self._vendors:
            self._vendors[name] = Vendor(name, self._raw)
        return self._vendors[name]

    def _get_or_create_suite(
        self,
        vendor: Vendor,
        name: str,
        create: bool = True
    ) -> Optional[Suite]:
        assert isinstance(name, str), repr(name)
        original_name = name

        suite = self._suites.get((str(vendor), name))

        if suite is not None:
            return suite

        raw: Optional[Dict[str, Any]] = None
        aliases: Set[str] = set()
        base: Optional[Suite] = None

        while True:
            for r in self._raw:
                p: Dict[str, Any] = r.get(
                    'vendors', {}
                ).get(str(vendor), {})
                raw = p.get('suites', {}).get(name)

                if raw:
                    break

            if raw is None or 'alias_for' not in raw:
                break

            name = raw['alias_for']
            if name in aliases:
                raise ConfigError(
                    '{!r}/{!r} is an alias for itself'.format(vendor, name))
            aliases.add(name)
            continue

        suite = self._suites.get((str(vendor), name))

        if suite is not None:
            return suite

        pattern = name

        if raw is None and '-' in name:
            base_name, pocket = name.split('-', 1)
            base = self.get_existing_suite(vendor, base_name)

            if base is not None:
                pattern = '*-{}'.format(pocket)
                for r in self._raw:
                    p = r.get('vendors', {}).get(str(vendor), {})
                    raw = p.get('suites', {}).get(pattern)

                    if raw is not None:
                        name = '{}-{}'.format(base, pocket)
                        break
                else:
                    pattern = name

        if raw is None and not create:
            return None

        for r in self._raw:
            p = r.get(
                'vendors', {}
            ).get(str(vendor), {})
            s: Dict[str, Any] = p.get(
                'suites', {}
            ).get(pattern, {})

            if 'base' in s:
                b = s['base']

                if '/' in b:
                    v, b = b.split('/', 1)
                    base_vendor = self.get_vendor(v)
                else:
                    base_vendor = vendor

                if '*' in b and base is not None:
                    b = b.replace('*', str(base))

                base = self.ensure_suite(base_vendor, b)
                break

        suite = Suite(name, vendor, self._raw, base=base, pattern=pattern)
        self._suites[(str(vendor), original_name)] = suite
        self._suites[(str(vendor), name)] = suite
        return suite

    def ensure_suite(self, vendor: Vendor, name: str) -> Suite:
        ret = self._get_or_create_suite(vendor, name, create=True)
        assert ret is not None
        return ret

    def get_existing_suite(self, vendor: Vendor, name: str) -> Optional[Suite]:
        return self._get_or_create_suite(vendor, name, create=False)


class Arguments(ConfigView):
    def __init__(self, subcommand: str, config: Config) -> None:
        super(Arguments, self).__init__()
        self._subcommand = subcommand
        self._config = config
        self._overrides: Dict[str, Any] = {}

        for name in ('http_proxy', 'https_proxy', 'ftp_proxy'):
            env = os.environ.get(name)

            if env is not None:
                self._overrides[name] = env

        env = os.environ.get('no_proxy')

        if env is not None:
            self._overrides['no_proxy'] = env.split(',')

    def copy(self) -> 'Arguments':
        other = Arguments(self._subcommand, self._config)
        other._overrides = dict(self._overrides)
        return other

    def can_override(self, name: str) -> bool:
        return self._config.can_override(name)

    def get_override(self, name: str) -> Any:
        return self._overrides[name]

    def is_overridden(self, name: str) -> bool:
        return name in self._overrides

    def override(self, name: str, value: Any) -> None:
        self._overrides[name] = value

    def _get(
        self,
        name: str,
        inherit_from_suite: bool = True,
        inherit_from_vendor: bool = True,
    ) -> Any:
        if name in self._overrides:
            return self._overrides[name]

        try:
            return self._config.path_based._get(name)
        except KeyError:
            pass

        if (
            inherit_from_suite
            and self.suite is not None
        ):
            return self.suite._get(name)

        if inherit_from_vendor:
            return self.vendor._get(name)

        return self._config._get(
            name,
            inherit_from_suite=inherit_from_suite,
            inherit_from_vendor=inherit_from_vendor,
        )

    def dump(self, stream: TextIO = sys.stdout) -> None:
        yaml.dump(self._overrides, stream)

    def get_vendor(self, name: str) -> Vendor:
        return self._config.get_vendor(name)

    def ensure_suite(self, vendor: Vendor, name: str) -> Suite:
        return self._config.ensure_suite(vendor, name)

    def get_existing_suite(self, vendor: Vendor, name: str) -> Optional[Suite]:
        return self._config.get_existing_suite(vendor, name)

    @property
    def sbuild_buildables(self) -> Sequence[str]:
        return self._get('sbuild_buildables')

    @property
    def refresh_vendors(self) -> Iterable[Vendor]:
        if self._get('refresh_vendors'):
            for v in self._get('refresh_vendors'):
                assert isinstance(v, str), repr(v)
                yield self.get_vendor(v)
        else:
            yield self.vendor

    @property
    def refresh_suites(self) -> Iterable[Suite]:
        suites = self._get(
            'refresh_suites',
            inherit_from_suite=False,
            inherit_from_vendor=False,
        )

        if suites:
            for s in suites:
                if '/' in s:
                    v, s = s.split('/', 1)
                    vendor = self.get_vendor(v)
                else:
                    vendor = self.vendor

                yield self.ensure_suite(vendor, s)

            return

        if self._get('refresh_vendors'):
            for vendor in self.refresh_vendors:
                for suite in vendor._get('refresh_suites'):
                    yield self.ensure_suite(vendor, suite)

            return

        if self.suite is not None:
            yield self.suite
        else:
            yield self.ensure_suite(self.vendor, self.vendor.default_suite)

    @property
    def refresh_architectures(self) -> Iterable[str]:
        archs = self._get('refresh_architectures')

        if archs:
            for a in archs:
                yield a
        else:
            yield self.architecture

    @property
    def subcommand(self) -> str:
        return self._subcommand

    @property
    def config(self) -> Config:
        return self._config
