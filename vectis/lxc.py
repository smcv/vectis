# Copyright © 2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import textwrap
from tempfile import TemporaryDirectory

from debian.debian_support import (
    Version,
)

from vectis.util import (
    AtomicWriter,
)
from vectis.worker import (
    VirtWorker,
)


def set_up_lxc_net(
    worker: VirtWorker,
    subnet: str,
) -> None:
    lxc_version = worker.dpkg_version('lxc')

    if lxc_version < Version('1:4.0.2-1'):
        with TemporaryDirectory(prefix='vectis-lxc-') as tmp:
            with AtomicWriter(os.path.join(tmp, 'lxc-net')) as writer:
                writer.write(textwrap.dedent('''\
                USE_LXC_BRIDGE="true"
                '''))

                if lxc_version >= Version('3'):
                    writer.write(textwrap.dedent('''\
                    LXC_BRIDGE="lxcbr0"
                    LXC_ADDR="{subnet}.1"
                    LXC_NETMASK="255.255.255.0"
                    LXC_NETWORK="{subnet}.0/24"
                    LXC_DHCP_RANGE="{subnet}.2,{subnet}.254"
                    LXC_DHCP_MAX="253"
                    LXC_DHCP_CONFILE=""
                    LXC_DOMAIN=""
                    ''').format(subnet=subnet))

            worker.copy_to_guest(
                os.path.join(tmp, 'lxc-net'), '/etc/default/lxc-net')

            with AtomicWriter(os.path.join(tmp, 'default.conf')) as writer:
                if lxc_version >= Version('3'):
                    writer.write(textwrap.dedent('''\
                    lxc.net.0.type = veth
                    lxc.net.0.link = lxcbr0
                    lxc.net.0.flags = up
                    lxc.net.0.hwaddr = 00:16:3e:xx:xx:xx
                    lxc.apparmor.profile = unconfined
                    '''))
                else:
                    writer.write(textwrap.dedent('''\
                    lxc.network.type = veth
                    lxc.network.link = lxcbr0
                    lxc.network.flags = up
                    lxc.network.hwaddr = 00:16:3e:xx:xx:xx
                    '''))

            worker.copy_to_guest(
                os.path.join(tmp, 'default.conf'), '/etc/lxc/default.conf')

        worker.check_call(['systemctl', 'enable', 'lxc-net'])
        worker.check_call(['systemctl', 'stop', 'lxc-net'])
        worker.check_call(['systemctl', 'start', 'lxc-net'])


def set_up_lxd_net(
    worker: VirtWorker,
    subnet: str,
):
    worker.check_call([
        'lxc', 'network', 'create', 'lxdbr0',
        'ipv4.address={subnet}.1/24'.format(subnet=subnet),
        'ipv4.nat=true',
        '--force-local',
    ])
    worker.check_call([
        'lxc', 'network', 'attach-profile', 'lxdbr0',
        'default', 'eth0',
        '--force-local',
    ])
