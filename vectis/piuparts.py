# Copyright © 2017 Simon McVittie
# Copyright © 2017 Collabora Ltd.
# SPDX-License-Identifier: GPL-2.0-or-later
# (see vectis/__init__.py)

import logging
import os
from contextlib import (
    ExitStack,
)
from typing import (
    Dict,
    Iterable,
    List,
    Optional,
    Sequence,
    Set,
    TYPE_CHECKING,
    Tuple,
)

from vectis.apt import (
    AptSource,
)
from vectis.worker import (
    ContainerWorker,
    FileProvider,
    VirtWorker,
)

if TYPE_CHECKING:
    import vectis.config

logger = logging.getLogger(__name__)


class Binary:

    def __init__(
        self,
        name: str,
        *,
        deb: Optional[str] = None
    ) -> None:
        self.deb = deb
        self.name = name

    def __str__(self) -> str:
        return self.name


class PiupartsWorker(FileProvider, ContainerWorker):

    def __init__(
        self,
        *,
        architecture: str,
        archive_access: 'vectis.config.ArchiveAccess',
        suite: 'vectis.config.Suite',
        tarball: str,
        worker: VirtWorker,
        components: Sequence[str] = (),
        extra_repositories: Sequence[str] = (),
        extra_repository_keys: Sequence[str] = ()
    ) -> None:
        super().__init__(archive_access=archive_access, suite=suite)

        if architecture is None:
            architecture = worker.dpkg_architecture

        self.__bound = set()            # type: Set[str]
        self.__cached_copies = {}       # type: Dict[Tuple[str, str], str]
        self.apt_related_argv = []      # type: List[str]
        self.argv = [
            'piuparts',
            '--arch',
            architecture,
            '-b',
            tarball,
        ]
        self.components = components
        self.extra_repositories = extra_repositories
        self.extra_repository_keys = extra_repository_keys
        self.worker = worker

        assert isinstance(self.worker, VirtWorker)

    def _open(self):
        super()._open()
        self.set_up_apt()

    def set_up_apt(self):
        argv = []

        for ancestor in self.suite.hierarchy:
            if self.components:
                filtered_components = (
                    set(self.components) & set(ancestor.all_components))
            else:
                filtered_components = ancestor.components

            uri = self.archive_access.mirror_for_suite(ancestor)

            source = AptSource(
                components=filtered_components,
                suite=ancestor.apt_suite,
                type='deb',
                trusted=ancestor.apt_trusted,
                uri=uri,
            )

            if ancestor is self.suite.hierarchy[-1]:
                logger.info(
                        '%r: %s => -d %s --mirror %s',
                        self, ancestor, source.suite,
                        source.get_piuparts_mirror_option())
                argv.append('-d')
                argv.append(source.suite)
                argv.append('--mirror')
                argv.append(source.get_piuparts_mirror_option())
            else:
                logger.info('%r: %s => %s', self, ancestor, source)
                argv.append('--extra-repo')
                argv.append(str(source))

        for line in self.extra_repositories:
            argv.append('--extra-repo')
            argv.append(line)

        self.apt_related_argv = argv
        self.install_apt_keys()

    def install_apt_key(self, apt_key):
        logger.debug('TODO: piuparts does not have an option to install '
                     'apt keys')

    def call_piuparts(
            self,
            *,
            binaries=(),
            changes=(),
            extra_options=(),
            ignore=(),
            ignore_regexes=(),
            output_dir=None):

        packages = []
        work_dir = None
        basenames_in_work_dir = set()

        for c in changes:
            d, f = self.make_changes_file_available(c)
            packages.append('{}/{}'.format(d, f))

        for b in binaries:
            if b.deb is None:
                packages.append(b.name)
            else:
                base = os.path.basename(b.deb)

                if base in basenames_in_work_dir:
                    # basename collision: give it its own directory
                    packages.append(self.make_file_available(b.deb))
                else:
                    if work_dir is None:
                        work_dir = self.new_directory()

                    packages.append(
                        self.make_file_available(b.deb, in_dir=work_dir),
                    )
                    basenames_in_work_dir.add(base)

        argv = self.argv[:]
        argv.append('--single-changes-list')

        for b in binaries:
            if b.deb is None:
                argv.append('--apt')
                break

        for i in ignore:
            argv.append('--ignore')
            argv.append(i)

        for i in ignore_regexes:
            argv.append('--ignore-regex')
            argv.append(i)

        if output_dir is not None:
            argv.append('-l')
            argv.append(output_dir + '/piuparts.log')

        argv.extend(self.apt_related_argv)
        argv.extend(extra_options)
        argv.extend(packages)

        return (self.worker.call(argv) == 0)

    def new_directory(self, prefix='', tmpdir=None):
        # assume /tmp is initially empty and mktemp won't collide
        d = self.worker.new_directory(prefix, tmpdir)
        self.argv.append('--bindmount={}'.format(d))
        self.__bound.add(d)
        return d

    def make_file_available(
            self,
            filename,
            *,
            cache=False,
            in_dir=None,
            owner=None):
        if in_dir is None:
            in_dir = self.new_directory()

        if cache:
            in_guest = self.__cached_copies.get((filename, in_dir))
            if in_guest is not None:
                return in_guest

        in_guest = self.worker.make_file_available(
            filename, cache=cache, in_dir=in_dir)

        if cache:
            self.__cached_copies[(filename, in_dir)] = in_guest

        return in_guest

    def make_dsc_file_available(self, filename, owner=None):
        d, f = self.worker.make_dsc_file_available(filename)
        self.argv.append('--bindmount={}'.format(d))
        return d, f

    def make_changes_file_available(self, filename, owner=None):
        d, f = self.worker.make_changes_file_available(filename)
        self.argv.append('--bindmount={}'.format(d))
        return d, f


def run_piuparts(
    *,
    archive_access: 'vectis.config.ArchiveAccess',
    components: Sequence[str],
    storage: str,
    suite: 'vectis.config.Suite',
    tarballs: Iterable[str],
    vendor: 'vectis.config.Vendor',
    worker: VirtWorker,
    architecture: Optional[str] = None,
    binaries: Iterable[Binary] = (),
    changes: Iterable[str] = (),
    extra_options: Iterable[str] = (),
    extra_repositories: Sequence[str] = (),
    extra_repository_keys: Sequence[str] = (),
    ignore: Iterable[str] = (),
    ignore_regexes: Iterable[str] = (),
    output_logs: Optional[str] = None
) -> Sequence[str]:
    failures = []   # type: List[str]
    # We may need to iterate these more than once
    binaries = list(binaries)
    changes = list(changes)

    with ExitStack() as stack:
        stack.enter_context(worker)
        worker.apt_install(['piuparts'])

        if architecture is None:
            architecture = worker.dpkg_architecture

        assert architecture is not None

        for basename in tarballs:
            tarball = os.path.join(
                storage,
                str(vendor),
                str(suite.hierarchy[-1]),
                architecture,
                basename,
            )

            if not os.path.exists(tarball):
                logger.info('Required tarball %s does not exist',
                            tarball)
                continue

            piuparts = stack.enter_context(
                PiupartsWorker(
                    architecture=architecture,
                    archive_access=archive_access,
                    components=components,
                    extra_repositories=extra_repositories,
                    extra_repository_keys=extra_repository_keys,
                    suite=suite,
                    tarball=worker.make_file_available(
                        tarball, cache=True),
                    worker=worker,
                )
            )

            for mode in ('install-purge',):
                if output_logs is None:
                    output_dir = None
                else:
                    output_dir = os.path.join(
                        output_logs,
                        'piuparts_{}_{}_{}'.format(
                            mode, basename, architecture))

                output_on_worker = worker.new_directory()

                if not piuparts.call_piuparts(
                        binaries=binaries,
                        changes=changes,
                        extra_options=extra_options,
                        ignore=ignore,
                        ignore_regexes=ignore_regexes,
                        output_dir=output_on_worker,
                ):
                    if output_dir is None:
                        failures.append(mode)
                    else:
                        failures.append(output_dir)

                if output_dir is not None:
                    worker.copy_to_host(
                        os.path.join(output_on_worker, ''),
                        os.path.join(output_dir, ''),
                    )

    return failures
