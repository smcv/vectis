# Copyright © 2016 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later


class Error(RuntimeError):
    pass


class ArgumentError(Error):
    pass


class CannotHappen(Error):
    pass


class NotSupported(Error):
    pass
