# Copyright © 2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
import time
from contextlib import suppress
from typing import (
    List,
    Optional,
    Sequence,
    TYPE_CHECKING,
)

from vectis.error import (
    ArgumentError,
)
from vectis.piuparts import (
    Binary,
    run_piuparts,
)
from vectis.worker import (
    VirtWorker,
)

if TYPE_CHECKING:
    import vectis.config

logger = logging.getLogger(__name__)


def _piuparts(
    things: Sequence[str],
    *,
    architecture: Optional[str],
    archive_access: 'vectis.config.ArchiveAccess',
    output_logs: str,
    storage: str,
    suite: 'vectis.config.Suite',
    tarballs: Sequence[str],
    vendor: 'vectis.config.Vendor',
    worker: VirtWorker,
    extra_options: Sequence[str] = (),
    extra_repositories: Sequence[str] = (),
    extra_repository_keys: Sequence[str] = (),
    ignore: Sequence[str] = (),
    ignore_regexes: Sequence[str] = (),
) -> Sequence[str]:
    binaries = []   # type: List[Binary]
    changes = []    # type: List[str]

    for thing in things:
        if os.path.exists(thing):
            if thing.endswith('.changes'):
                changes.append(thing)
            elif thing.endswith('.deb'):
                binaries.append(Binary(thing, deb=thing))
        else:
            binaries.append(Binary(thing))

    return run_piuparts(
        architecture=architecture,
        binaries=binaries,
        changes=changes,
        components=(),
        extra_options=extra_options,
        extra_repositories=extra_repositories,
        extra_repository_keys=extra_repository_keys,
        ignore=ignore,
        ignore_regexes=ignore_regexes,
        archive_access=archive_access,
        output_logs=output_logs,
        storage=storage,
        suite=suite,
        tarballs=tarballs,
        vendor=vendor,
        worker=worker,
    )


def run(
    args,
    *,
    piuparts_options=[],
    _extra_repository=[],
    _extra_repository_keys=[],
    _things=[],
):
    if args.suite is None:
        if args.default_suite is not None:
            args.override('suite', args.default_suite)
        else:
            raise ArgumentError('--suite must be specified')

    output_dir = args.output_dir

    if output_dir is None:
        output_parent = args.output_parent
        assert output_parent is not None
        timestamp = time.strftime('%Y%m%dt%H%M%S', time.gmtime())
        output_dir = os.path.join(
            output_parent, 'piuparts_{}'.format(timestamp)
        )

    with suppress(FileNotFoundError):
        os.rmdir(output_dir)

    os.makedirs(output_dir)

    worker = VirtWorker(
        args.piuparts_worker,
        archive_access=args.get_archive_access(),
        storage=args.storage,
        suite=args.piuparts_worker_suite,
    )
    failures = _piuparts(
        _things,
        architecture=args.architecture,
        extra_options=piuparts_options,
        extra_repositories=_extra_repository,
        extra_repository_keys=_extra_repository_keys,
        ignore=args.piuparts_ignore,
        ignore_regexes=args.piuparts_ignore_regexes,
        archive_access=args.get_archive_access(),
        output_logs=output_dir,
        storage=args.storage,
        suite=args.suite,
        tarballs=args.piuparts_tarballs,
        vendor=args.vendor,
        worker=worker,
    )

    for failure in sorted(failures):
        logger.error('%s failed testing', failure)

    logger.info('Output directory: %s', output_dir)
