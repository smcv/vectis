# Copyright © 2016-2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
import shlex
import subprocess
from tempfile import TemporaryDirectory
from typing import (
    Iterable,
    List,
    Optional,
    Sequence,
    TYPE_CHECKING,
    Tuple,
)

from debian.debian_support import (
    Version,
)

from vectis.error import ArgumentError
from vectis.util import (
    AtomicWriter,
    format_byte_size_iec,
    format_byte_size_traditional,
)
from vectis.worker import (
    VirtWorker,
)

if TYPE_CHECKING:
    import vectis.config
    vectis.config       # appease pyflakes

logger = logging.getLogger(__name__)


def vmdebootstrap_argv(
    version: Version,
    *,
    architecture: str,
    archive_access: 'vectis.config.ArchiveAccess',
    components: Iterable[str],
    debootstrap_version: Version,
    include: Sequence[str] = (),
    kernel_package: Optional[str],
    merged_usr: Optional[bool],
    qemu_image_size: int,
    suite: 'vectis.config.Suite',
    uri: str
) -> Tuple[List[str], List[str], str]:
    default_name = 'autopkgtest.qcow2'
    argv = [
        'env',
        'AUTOPKGTEST_APT_PROXY={}'.format(
            archive_access.apt_proxy_for_suite(suite)),
        'MIRROR={}'.format(uri),
        'RELEASE={}'.format(suite),

        'vmdebootstrap',
        '--log=/dev/stderr',
        '--verbose',
        '--serial-console',
        '--distribution={}'.format(suite),
        '--user=user',
        '--hostname=host',
        '--sparse',
        '--size={}'.format(format_byte_size_iec(qemu_image_size)),
        '--mirror={}'.format(uri),
        '--arch={}'.format(architecture),
        '--grub',
        '--no-extlinux',
    ]

    if kernel_package is not None:
        if version >= Version('1.4'):
            argv.append('--kernel-package={}'.format(kernel_package))
        else:
            argv.append('--no-kernel')
            argv.append('--package={}'.format(kernel_package))

    for package in include:
        argv.append('--package={}'.format(package))

    debootstrap_args = []   # type: List[str]

    debootstrap_args.append('components={}'.format(
        ','.join(components)))

    if debootstrap_version >= Version('1.0.86~'):
        if suite.merged_usr_required:
            if merged_usr is False:
                raise ArgumentError(
                    f'Suite {suite} does not support non-merged-/usr'
                )

            merged_usr = True
        elif merged_usr:
            if not suite.merged_usr_allowed:
                raise ArgumentError(
                    f'Suite {suite} did not support merged-/usr'
                )

            basename = 'autopkgtest-merged-usr.qcow2'

        if merged_usr:
            debootstrap_args.append('merged-usr')
        else:
            debootstrap_args.append('no-merged-usr')

    return argv, debootstrap_args, default_name


def new_vmdb2(
    *,
    apt_key: str,
    apt_key_package: str,
    architecture: str,
    archive_access: 'vectis.config.ArchiveAccess',
    components: Iterable[str],
    default_dir: str,
    include: Sequence[str] = (),
    # TODO? kernel_package: str,
    merged_usr: Optional[bool],
    out: Optional[str],
    qemu_image_size: int,
    storage: str,
    suite: 'vectis.config.Suite',
    uri: str,
    vendor: 'vectis.config.Vendor',
    # TODO? vmdb2_options: Sequence[str],
    vmdb2_worker: str,
    vmdb2_worker_suite: 'vectis.config.Suite',
) -> Tuple[str, str]:
    if out is None:
        out = os.path.join(default_dir, 'autopkgtest.qcow2')

    for suite in (vmdb2_worker_suite, suite):
        for ancestor in suite.hierarchy:
            archive_access.check_suite(ancestor)

    include = list(include)

    if apt_key_package:
        include.append(apt_key_package)

    with VirtWorker(
        vmdb2_worker,
        archive_access=archive_access,
        storage=storage,
        suite=vmdb2_worker_suite,
    ) as worker, TemporaryDirectory(
        prefix='vectis-new-',
    ) as local_temp:
        worker.check_call([
            'env', 'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env() + [
            'apt-get', '-y', 'upgrade',
        ])

        worker_packages = [
            'autopkgtest',
            'vmdb2',
        ]

        if worker.dpkg_version('base-files') >= Version(13):
            worker_packages.append('zerofree')

        worker.apt_install(worker_packages, recommends=False)
        autopkgtest_version = worker.dpkg_version('autopkgtest')

        # vmdb2 doesn't have a way to pass arbitrary options through to
        # debootstrap, so we install a shim version of debootstrap that
        # has those options pre-specified.
        their_debootstrap = worker.check_output([
            'sh', '-euc', 'command -v debootstrap',
        ]).decode('utf-8').rstrip('\n')
        our_debootstrap = os.path.join(local_temp, 'debootstrap')

        escaped_args = [
            their_debootstrap,
            '--components=' + shlex.quote(','.join(components)),
            '--include=' + shlex.quote(','.join(include)),
            '--keyring=' + worker.make_file_available(apt_key),
            {
                True: '--merged-usr',
                False: '--no-merged-usr',
                None: '',
            }.get(merged_usr, ''),
        ]

        with AtomicWriter(our_debootstrap) as writer:
            writer.write('#!/bin/sh\n')
            writer.write('set -eux\n')
            writer.write(
                'exec {} "$@"\n'.format(
                    ' '.join(escaped_args),
                )
            )

        their_vmdb2 = worker.check_output([
            'sh', '-euc', 'command -v vmdb2',
        ]).decode('utf-8').rstrip('\n')
        our_vmdb2 = os.path.join(local_temp, 'vmdb2')

        escaped_args = [
            their_vmdb2,
            '--log', '{}/vmdb2.log'.format(worker.scratch),
        ]

        with AtomicWriter(our_vmdb2) as writer:
            writer.write('#!/bin/sh\n')
            writer.write('set -eux\n')
            writer.write(
                'exec {} "$@"\n'.format(
                    ' '.join(escaped_args),
                )
            )

        setup_script = os.path.join(local_temp, 'setup-script')

        with AtomicWriter(setup_script) as writer:
            writer.write('#!/bin/sh\n')
            writer.write('set -eux\n')
            writer.write('root="$1"\n')

        remote_bin = worker.new_directory(prefix='bin')

        debootstrap = '{}/debootstrap'.format(remote_bin)
        worker.copy_to_guest(our_debootstrap, debootstrap)
        worker.check_call(['chmod', '+x', debootstrap])

        vmdb2 = '{}/vmdb2'.format(remote_bin)
        worker.copy_to_guest(our_vmdb2, vmdb2)
        worker.check_call(['chmod', '+x', vmdb2])

        vmdb_setup_script = '{}/vmdb2-setup-script'.format(remote_bin)
        worker.copy_to_guest(setup_script, vmdb_setup_script)
        worker.check_call(['chmod', '+x', vmdb_setup_script])

        path = worker.check_output([
            'sh', '-euc',
            'printf -- \'%s\' "${PATH:-/usr/sbin:/usr/bin:/sbin:/bin}"',
        ]).decode('utf-8')
        path_components = path.split(':')

        for d in ('/usr/sbin', '/usr/bin', '/sbin', '/bin'):
            if d not in path_components:
                path += f'{path}:{d}'

        argv = [
            str(suite),
            '{}/output.qcow2'.format(worker.scratch),
            uri,
            architecture,
            vmdb_setup_script,
        ]

        if autopkgtest_version >= Version('5.11'):
            argv[:0] = [
                '--size=' + format_byte_size_traditional(qemu_image_size),
            ]
        else:
            logger.warning('Unable to set size for image')

        assert worker.command_wrapper is not None
        try:
            worker.check_call([
                'env',
                'AUTOPKGTEST_APT_PROXY={}'.format(
                    archive_access.apt_proxy_for_suite(suite)),
                'DEBIAN_FRONTEND=noninteractive',
                'PATH={}:{}'.format(remote_bin, path),
                worker.command_wrapper,
                '--',
                'autopkgtest-build-qemu',
            ] + argv)
        except subprocess.CalledProcessError:
            worker.call(['cat', '{}/vmdb2.log'.format(worker.scratch)])
            raise
        else:
            worker.call(['cat', '{}/vmdb2.log'.format(worker.scratch)])

        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/output.qcow2'.format(worker.scratch), out + '.new')

    return out + '.new', out


def new_ubuntu_cloud(
    *,
    architecture: str,
    default_dir: str,
    out: Optional[str],
    qemu_image_size: int,
    suite: 'vectis.config.Suite',
    uri: str,
    vendor: 'vectis.config.Vendor'
):
    if out is None:
        out = os.path.join(default_dir, 'autopkgtest-cloud.qcow2')

    out_dir = os.path.dirname(out) or os.curdir
    os.makedirs(out_dir, exist_ok=True)

    completed = subprocess.run(
        ['autopkgtest-buildvm-ubuntu-cloud', '--help'],
        stdout=subprocess.PIPE,
        universal_newlines=True,
    )
    help_output = completed.stdout
    assert help_output is not None

    argv = ['autopkgtest-buildvm-ubuntu-cloud']

    argv.append('--arch={}'.format(architecture))
    argv.append(
        '--disk-size={}'.format(format_byte_size_traditional(qemu_image_size)))
    argv.append('--mirror={}'.format(uri))
    argv.append('--proxy=DIRECT')
    argv.append('--release={}'.format(suite))
    argv.append('--verbose')
    argv.append('--output-dir={}'.format(out_dir))

    if '[--compress]' in help_output:
        argv.append('--compress')

    image = '{}/autopkgtest-{}-{}.img'.format(out_dir, suite, architecture)

    try:
        subprocess.check_call(argv)
    except Exception:
        if os.path.exists(image):
            os.unlink(image)
        raise
    else:
        return image, out


def new(
    *,
    apt_key: str,
    apt_key_package: str,
    architecture: str,
    archive_access: 'vectis.config.ArchiveAccess',
    components: Iterable[str],
    default_dir: str,
    include: Sequence[str] = (),
    kernel_package: str,
    merged_usr: Optional[bool],
    out: Optional[str],
    qemu_image_size: int,
    storage: str,
    suite: 'vectis.config.Suite',
    uri: str,
    vmdebootstrap_options: Sequence[str],
    vmdebootstrap_worker: str,
    vmdebootstrap_worker_suite: 'vectis.config.Suite'
):

    for suite in (vmdebootstrap_worker_suite, suite):
        for ancestor in suite.hierarchy:
            archive_access.check_suite(ancestor)

    with VirtWorker(
        vmdebootstrap_worker,
        archive_access=archive_access,
        storage=storage,
        suite=vmdebootstrap_worker_suite,
    ) as worker:
        worker.check_call([
            'env', 'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env() + [
            'apt-get', '-y', 'upgrade',
        ])

        worker_packages = [
            'autopkgtest',
            'grub2-common',
            'python3',
            'qemu-utils',
            'vmdebootstrap',
        ]

        # Optional (x86 only, but necessary for wheezy)
        optional_worker_packages = [
            'extlinux',
            'mbr',
        ]

        keyring = apt_key_package

        if keyring is not None:
            optional_worker_packages.append(keyring)

        worker.apt_install(worker_packages, recommends=False)

        # Failure is ignored for these non-critical packages
        for p in optional_worker_packages:
            worker.apt_install([p], may_fail=True, recommends=False)

        version = worker.dpkg_version('vmdebootstrap')
        debootstrap_version = worker.dpkg_version('debootstrap')

        argv, debootstrap_args, default_name = vmdebootstrap_argv(
            version,
            architecture=architecture,
            archive_access=archive_access,
            components=components,
            debootstrap_version=debootstrap_version,
            include=include,
            kernel_package=kernel_package,
            qemu_image_size=qemu_image_size,
            suite=suite,
            uri=uri,
            merged_usr=merged_usr,
        )
        argv.extend(vmdebootstrap_options)

        if worker.call(['test', '-f', apt_key]) == 0:
            logger.info('Found apt key worker:{}'.format(apt_key))
            debootstrap_args.append('keyring={}'.format(apt_key))
        elif os.path.exists(apt_key):
            logger.info('Found apt key host:{}, copying to worker:{}'.format(
                apt_key, '{}/apt-key.gpg'.format(worker.scratch)))
            worker.copy_to_guest(
                apt_key, '{}/apt-key.gpg'.format(worker.scratch))
            debootstrap_args.append('keyring={}/apt-key.gpg'.format(
                worker.scratch))
        else:
            logger.warning('Apt key host:{} not found; leaving it out and '
                           'hoping for the best'.format(apt_key))

        if debootstrap_args:
            argv.append('--debootstrapopts={}'.format(
                ' '.join(debootstrap_args)))

        command_wrapper = worker.command_wrapper
        assert command_wrapper is not None      # TODO: is this guaranteed?

        worker.copy_to_guest(
            os.path.join(os.path.dirname(__file__), '..', 'setup-testbed'),
            '{}/setup-testbed'.format(worker.scratch))
        worker.check_call([
            'chmod', '0755', '{}/setup-testbed'.format(worker.scratch)])
        worker.check_call([
            'env',
            'DEBIAN_FRONTEND=noninteractive',
            'AUTOPKGTEST_APT_PROXY=' + archive_access.http_proxy,
            'AUTOPKGTEST_KEEP_APT_SOURCES=1',
        ] + archive_access.get_proxy_env() + [
            command_wrapper,
            '--',
        ] + argv + [
            '--customize={}/setup-testbed'.format(worker.scratch),
            '--image={}/output.raw'.format(worker.scratch),
        ])

        worker.check_call([
            'qemu-img', 'convert', '-f', 'raw', '-O',
            'qcow2', '-c', '-p',
            '{}/output.raw'.format(worker.scratch),
            '{}/output.qcow2'.format(worker.scratch),
        ])

        if out is None:
            out = os.path.join(default_dir, default_name)

        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/output.qcow2'.format(worker.scratch), out + '.new')

    return out + '.new', out


def run(
    args,
    *,
    _cloud=False,
    _include=[],
    _keep=False,
    _merged_usr: Optional[bool] = None,
    _output=None,
    _uri=None,
    _vmdb2=True,
):
    if args.suite is None:
        if args.default_suite is not None:
            args.override('suite', args.default_suite)
        else:
            raise ArgumentError('--suite must be specified')

    apt_key = args.apt_key
    apt_key_package = args.apt_key_package
    architecture = args.architecture
    archive_access = args.get_archive_access()
    cloud = _cloud
    components = args.components
    keep = _keep
    kernel_package = args.get_kernel_package(architecture)
    include = _include
    out = _output

    if out is None:
        out = args.write_qemu_image

    qemu_image_size = args.qemu_image_size
    storage = args.storage
    uri = _uri
    vendor = args.vendor
    suite = args.suite
    assert suite is not None
    vmdebootstrap_options = suite.vmdebootstrap_options
    default_dir = os.path.join(
        storage,
        str(vendor),
        str(suite),
        architecture,
    )

    if uri is None:
        uri = archive_access.mirror_for_suite(suite)

    for ancestor in suite.hierarchy:
        archive_access.check_suite(ancestor)

    if cloud:
        created, out = new_ubuntu_cloud(
            architecture=architecture,
            default_dir=default_dir,
            out=out,
            qemu_image_size=qemu_image_size,
            suite=suite,
            uri=uri,
            vendor=vendor,
        )
    elif _vmdb2:
        for suffix in (
            '', '_architecture', '_qemu_image', '_suite', '_vendor'
        ):
            if args.is_overridden('worker' + suffix):
                args.override(
                    'vmdb2_worker' + suffix,
                    args.get_override('worker' + suffix)
                )

        worker = args.vmdb2_worker
        worker_suite = args.vmdb2_worker_suite

        for ancestor in args.worker_suite.hierarchy:
            archive_access.check_suite(ancestor)

        created, out = new_vmdb2(
            apt_key=apt_key,
            apt_key_package=apt_key_package,
            architecture=architecture,
            archive_access=archive_access,
            components=components,
            default_dir=default_dir,
            include=include,
            merged_usr=_merged_usr,
            out=out,
            qemu_image_size=qemu_image_size,
            storage=storage,
            suite=suite,
            uri=uri,
            vendor=vendor,
            vmdb2_worker=worker,
            vmdb2_worker_suite=worker_suite,
        )
    else:
        for suffix in (
            '', '_architecture', '_qemu_image', '_suite', '_vendor'
        ):
            if args.is_overridden('worker' + suffix):
                args.override(
                    'vmdebootstrap_worker' + suffix,
                    args.get_override('worker' + suffix)
                )

        worker = args.vmdebootstrap_worker
        worker_suite = args.vmdebootstrap_worker_suite

        for ancestor in worker_suite.hierarchy:
            archive_access.check_suite(ancestor)

        created, out = new(
            apt_key=apt_key,
            apt_key_package=apt_key_package,
            architecture=architecture,
            archive_access=archive_access,
            components=components,
            default_dir=default_dir,
            kernel_package=kernel_package,
            include=include,
            merged_usr=_merged_usr,
            out=out,
            qemu_image_size=qemu_image_size,
            storage=storage,
            suite=suite,
            uri=uri,
            vmdebootstrap_options=vmdebootstrap_options,
            vmdebootstrap_worker=worker,
            vmdebootstrap_worker_suite=worker_suite,
        )

    try:
        with VirtWorker(
            ['qemu', created],
            archive_access=archive_access,
            storage=storage,
            suite=suite,
        ) as test_worker:
            test_worker.set_up_apt()
            test_worker.check_call(
                archive_access.get_proxy_env() + ['apt-get', '-y', 'update'])
            test_worker.check_call([
                'env',
                'DEBIAN_FRONTEND=noninteractive',
            ] + archive_access.get_proxy_env() + [
                'apt-get',
                '-y',
                '--no-install-recommends',
                'install',

                'python3',
                'sbuild',
                'schroot',
            ])
    except Exception:
        if keep:
            if created != out + '.new':
                os.rename(created, out + '.new')
        else:
            os.remove(created)

        raise
    else:
        os.rename(created, out)
