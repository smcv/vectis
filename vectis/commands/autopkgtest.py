# Copyright © 2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
import time
from contextlib import suppress
from typing import List, Optional

from debian.deb822 import (
    Changes,
    Dsc,
)

from vectis.autopkgtest import (
    run_autopkgtest,
)
from vectis.error import (
    ArgumentError,
)
from vectis.worker import (
    VirtWorker,
)

logger = logging.getLogger(__name__)


class Source:

    def __init__(
        self,
        name: str,
        *,
        dsc: Optional[Dsc] = None
    ) -> None:
        self.name = name

        if os.path.isdir(name):
            self.dir = name     # type: Optional[str]
        else:
            self.dir = None

        self.dsc = dsc
        self.failures = []      # type: List[str]

    def __str__(self) -> str:
        return self.name


def _autopkgtest(
    things,
    *,
    architecture,
    archive_access,
    built_binaries,
    lxc_24bit_subnet,
    lxc_worker,
    lxd_worker,
    modes,
    output_logs,
    qemu_overlay_dir,
    qemu_ram_size,
    schroot_worker,
    storage,
    suite,
    vendor,
    worker,
    extra_options=(),
    extra_repositories=(),
    extra_repository_keys=()
):
    binaries = []
    sources = []

    for thing in things:
        if os.path.exists(thing):
            if thing.endswith('.changes'):
                with open(thing) as reader:
                    c = Changes(reader)

                    for f in c['files']:
                        n = os.path.join(
                            os.path.dirname(thing) or os.curdir, f['name'])

                        if f['name'].endswith('.deb'):
                            binaries.append(n)
                        elif f['name'].endswith('.dsc'):
                            sources.append(Source(n, dsc=Dsc(open(n))))

            elif thing.endswith('.dsc'):
                sources.append(Source(thing, dsc=Dsc(open(thing))))

            elif thing.endswith('.deb'):
                binaries.append(thing)

            elif os.path.isdir(thing):
                sources.append(Source(thing))
        else:
            sources.append(Source(thing))

    failures = set()

    for source in sources:
        source_dir = None
        source_dsc = None
        source_package = None

        if source.dsc is not None:
            source_dsc = source.name
        elif source.dir is not None:
            source_dir = source.dir
        else:
            source_package = source.name

        if built_binaries is None:
            built_binaries = not binaries

        for failure in run_autopkgtest(
                architecture=architecture,
                archive_access=archive_access,
                binaries=binaries,
                built_binaries=built_binaries,
                components=(),
                extra_options=extra_options,
                extra_repositories=extra_repositories,
                extra_repository_keys=extra_repository_keys,
                lxc_24bit_subnet=lxc_24bit_subnet,
                lxc_worker=lxc_worker,
                lxd_worker=lxd_worker,
                modes=modes,
                output_logs=output_logs,
                qemu_overlay_dir=qemu_overlay_dir,
                qemu_ram_size=qemu_ram_size,
                schroot_worker=schroot_worker,
                source_dir=source_dir,
                source_dsc=source_dsc,
                source_package=source_package,
                storage=storage,
                suite=suite,
                vendor=vendor,
                worker=worker,
        ):
            source.failures.append(failure)
            failures.add(source)

    return failures


def run(
    args,
    *,
    _built_binaries=None,
    _extra_repository=[],
    _extra_repository_keys=[],
    _things=[],
    autopkgtest_options=[],
    really=True,
):
    if args.suite is None:
        if args.default_suite is not None:
            args.override('suite', args.default_suite)
        else:
            raise ArgumentError('--suite must be specified')

    archive_access = args.get_archive_access()

    worker = VirtWorker(
        args.worker,
        archive_access=archive_access,
        storage=args.storage,
        suite=args.worker_suite,
    )

    if (args.lxc_worker == args.worker and
            args.lxc_worker_suite == args.worker_suite):
        lxc_worker = worker
    else:
        lxc_worker = VirtWorker(
            args.lxc_worker,
            archive_access=archive_access,
            storage=args.storage,
            suite=args.lxc_worker_suite,
        )

    if (args.lxd_worker == args.worker and
            args.lxd_worker_suite == args.worker_suite):
        lxd_worker = worker
    else:
        lxd_worker = VirtWorker(
            args.lxd_worker,
            archive_access=archive_access,
            storage=args.storage,
            suite=args.lxd_worker_suite,
        )

    output_dir = args.output_dir

    if output_dir is None:
        output_parent = args.output_parent
        assert output_parent is not None
        timestamp = time.strftime('%Y%m%dt%H%M%S', time.gmtime())
        output_dir = os.path.join(
            output_parent, 'autopkgtest_{}'.format(timestamp)
        )

    with suppress(FileNotFoundError):
        os.rmdir(output_dir)

    os.makedirs(output_dir)

    failures = _autopkgtest(
        _things,
        architecture=args.architecture,
        archive_access=archive_access,
        built_binaries=_built_binaries,
        extra_options=autopkgtest_options,
        extra_repositories=_extra_repository,
        extra_repository_keys=_extra_repository_keys,
        lxc_24bit_subnet=args.lxc_24bit_subnet,
        lxc_worker=lxc_worker,
        lxd_worker=lxd_worker,
        worker=worker,
        modes=args.autopkgtest,
        output_logs=output_dir,
        qemu_overlay_dir=args.qemu_overlay_dir,
        qemu_ram_size=args.qemu_ram_size,
        # use the misc worker instead of a specific schroot worker
        schroot_worker=None,
        storage=args.storage,
        suite=args.suite,
        vendor=args.vendor,
    )

    for failure in sorted(failures):
        logger.error('%s failed testing: %s', failure, failure.failures)

    logger.info('Output directory: %s', output_dir)
