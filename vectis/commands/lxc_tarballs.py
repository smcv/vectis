# Copyright © 2016-2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
from typing import Optional, TYPE_CHECKING

from vectis.lxc import (
    set_up_lxc_net,
)
from vectis.worker import (
    VirtWorker,
)

if TYPE_CHECKING:
    import vectis.config        # noqa

logger = logging.getLogger(__name__)


def run(
    args: 'vectis.config.Arguments',
    *,
    _security_uri: Optional[str] = None,
    _uri: Optional[str] = None,
) -> None:
    if args.suite is None:
        args.override('suite', args.default_suite)

    architecture = args.architecture
    archive_access = args.get_archive_access()
    security_uri = _security_uri
    storage = args.storage
    suite = args.suite
    assert suite is not None
    uri = _uri
    vendor = args.vendor
    worker_argv = args.lxc_worker
    worker_suite = args.lxc_worker_suite
    assert worker_suite is not None     # TODO

    apt_key_package = args.apt_key_package
    lxc_24bit_subnet = args.lxc_24bit_subnet

    for suite in (worker_suite, suite):
        for ancestor in suite.hierarchy:
            archive_access.check_suite(ancestor)

    os.makedirs(storage, exist_ok=True)

    rootfs_tarball = '{vendor}/{suite}/{arch}/lxc-rootfs.tar.gz'.format(
        arch=architecture,
        vendor=vendor,
        suite=suite,
    )
    meta_tarball = '{vendor}/{suite}/{arch}/lxc-meta.tar.gz'.format(
        arch=architecture,
        vendor=vendor,
        suite=suite,
    )
    logger.info('Creating tarballs %s, %s...', rootfs_tarball, meta_tarball)

    with VirtWorker(
        worker_argv,
        archive_access=archive_access,
        storage=storage,
        suite=worker_suite,
    ) as worker:
        logger.info('Installing debootstrap etc.')
        worker.apt_install([
            'debootstrap', 'lxc', 'lxc-templates', 'python3',
        ])
        set_up_lxc_net(worker, lxc_24bit_subnet)

        # FIXME: The lxc templates only allow installing the apt keyring
        # to use, and do not allow passing --keyring to debootstrap
        keyring = apt_key_package

        if keyring is not None:
            worker.apt_install([keyring], may_fail=True, recommends=False)

        # FIXME: This is silly, but it's a limitation of the lxc templates.
        # We have to provide exactly two apt URLs.
        security_suite = args.ensure_suite(vendor, str(suite) + '-security')

        if uri is None:
            uri = archive_access.mirror_for_suite(suite)

        if security_uri is None:
            security_uri = archive_access.mirror_for_suite(security_suite)

        argv = [
            'env',
            'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env()

        command_wrapper = worker.command_wrapper

        if command_wrapper is not None:
            argv.extend([command_wrapper, '--'])

        argv.extend([
            'lxc-create',
            '--template={}'.format(vendor),
            '--name={}-{}-{}'.format(vendor, suite, architecture),
            '--',
            '--release={}'.format(suite),
            '--arch={}'.format(architecture),
            '--mirror={}'.format(uri),
            '--security-mirror={}'.format(security_uri),
        ])

        if str(vendor) == 'ubuntu':
            argv.append('--variant=minbase')

        worker.check_call(argv)

        worker.check_call([
            'tar', '-C',
            '/var/lib/lxc/{}-{}-{}/rootfs'.format(vendor, suite, architecture),
            '-f', '{}/rootfs.tar.gz'.format(worker.scratch),
            '--exclude=./var/cache/apt/archives/*.deb',
            '-z', '-c', '.',
        ])
        worker.check_call([
            'tar', '-C',
            '/var/lib/lxc/{}-{}-{}'.format(vendor, suite, architecture),
            '-f', '{}/meta.tar.gz'.format(worker.scratch),
            '-z', '-c', 'config',
        ])

        out = os.path.join(storage, rootfs_tarball)
        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/rootfs.tar.gz'.format(worker.scratch), out + '.new')
        # FIXME: smoke-test it?
        os.rename(out + '.new', out)

        out = os.path.join(storage, meta_tarball)
        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/meta.tar.gz'.format(worker.scratch), out + '.new')
        # FIXME: smoke-test it?
        os.rename(out + '.new', out)

    logger.info('Created tarballs %s, %s', rootfs_tarball, meta_tarball)
