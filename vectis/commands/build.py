# Copyright © 2016-2020 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
import subprocess

from vectis.config import (
    ConfigError,
    Suite,
)
from vectis.debuild import (
    BuildGroup,
    package_sources_to_debs,
)

logger = logging.getLogger(__name__)


def _summarize(buildables):
    for buildable in buildables:
        logger.info(
            'Built changes files from %s:\n\t%s',
            buildable,
            '\n\t'.join(sorted(buildable.changes_produced.values())),
        )

        logger.info(
            'Build logs from %s:\n\t%s',
            buildable,
            '\n\t'.join(sorted(buildable.logs.values())),
        )


def _lintian(buildables):
    for buildable in buildables:
        # Run lintian near the end for better visibility
        if buildable.changes_produced:
            subprocess.call(
                ['lintian', '-I', '-i']
                + list(buildable.changes_produced.values()))


def _publish(
        buildables,
        reprepro_dir,
        default_reprepro_suite=None):
    for buildable in buildables:
        if buildable.changes_produced:
            reprepro_suite = default_reprepro_suite

            if reprepro_suite is None:
                reprepro_suite = buildable.nominal_suite

            subprocess.call([
                'reprepro', '-b', reprepro_dir,
                'removesrc', str(reprepro_suite),
                buildable.source_package,
            ])

            for arch, changes in buildable.changes_produced.items():
                reprepro_suite = default_reprepro_suite

                if reprepro_suite is None:
                    reprepro_suite = buildable.nominal_suite

                ret = subprocess.call([
                    'reprepro', '--ignore=wrongdistribution',
                    '--ignore=missingfile',
                    '-b', reprepro_dir, 'include',
                    str(reprepro_suite),
                    os.path.join(buildable.output_dir, changes),
                ])

                # Work around #920377 in reprepro: if we can't
                # include a source-only changes file, include the .dsc
                # instead
                if ret != 0 and buildable.dsc_name is not None:
                    subprocess.call([
                        'reprepro', '--ignore=wrongdistribution',
                        '--ignore=missingfile',
                        '-b', reprepro_dir, 'includedsc',
                        str(reprepro_suite),
                        buildable.dsc_name,
                    ])


def run(
    args,
    *,
    _add_build_profile=[],
    _add_deb_build_option=[],
    _append_to_version=None,
    _archs=[],
    build_method='sbuild',
    _build_profiles=None,
    _build_source=None,
    _buildables=[],
    extra_packages=[],
    _extra_repository=[],
    _extra_repository_keys=[],
    extra_test_repository=[],
    extra_test_repository_keys=[],
    _include_orig_source=None,
    _indep=False,
    _pbuilder_options=[],
    _reprepro_dir=None,
    _reprepro_suite=None,
    _sbuild_options=[],
    _sbuild_tarball=None,
    _source_only=False,
    _u_ignored=None,
    _versions_since=None,
):
    if args.subcommand in ('pbuilder', 'sbuild'):
        build_method = args.subcommand

    if build_method == 'pbuilder':
        for suffix in (
            '', '_architecture', '_qemu_image', '_suite', '_vendor'
        ):
            if args.is_overridden('worker' + suffix):
                args.override(
                    'pbuilder_worker' + suffix,
                    args.get_override('worker' + suffix)
                )

        if _sbuild_options:
            logger.warning('--sbuild-option not applicable with pbuilder')

        if _sbuild_tarball is not None:
            logger.warning('--sbuild-tarball not applicable with pbuilder')

        if _build_source is not None:
            raise ConfigError(
                '--rebuild-source, --no-source not available with pbuilder',
            )

        if extra_packages:
            raise ConfigError(
                '--extra-package not available with pbuilder',
            )

        if _source_only:
            raise ConfigError(
                '--source-only not available with pbuilder',
            )
    elif build_method == 'sbuild':
        for suffix in (
            '', '_architecture', '_qemu_image', '_suite', '_vendor'
        ):
            if args.is_overridden('worker' + suffix):
                args.override(
                    'sbuild_worker' + suffix,
                    args.get_override('worker' + suffix)
                )

        if _pbuilder_options:
            logger.warning('--pbuilder-option not applicable with sbuild')

    deb_build_options = set()

    if 'DEB_BUILD_OPTIONS' in os.environ:
        for arg in os.environ['DEB_BUILD_OPTIONS'].split():
            deb_build_options.add(arg)

    for arg in _add_deb_build_option:
        deb_build_options.add(arg)

    for arg in deb_build_options:
        if arg == 'parallel' or arg.startswith('parallel='):
            break
    else:
        deb_build_options.add('parallel={}'.format(args.parallel))

    profiles = set()

    if _build_profiles is not None:
        for arg in _build_profiles.split(','):
            profiles.add(arg)
    elif 'DEB_BUILD_PROFILES' in os.environ:
        for arg in os.environ['DEB_BUILD_PROFILES'].split():
            profiles.add(arg)

    for arg in _add_build_profile:
        profiles.add(arg)

    db_options = []

    if _versions_since:
        db_options.append('-v{}'.format(_versions_since))

    if _include_orig_source is not None:
        MAP = {
            'yes': 'a',
            'always': 'a',
            'force': 'a',
            'a': 'a',

            'auto': 'i',
            'maybe': 'i',
            'i': 'i',

            'no': 'd',
            'never': 'd',
            'd': 'd',
        }

        db_options.append('-s{}'.format(MAP[_include_orig_source]))

    ds_options = []

    if args.dpkg_source_diff_ignore is ...:
        ds_options.append('-i')
    elif args.dpkg_source_diff_ignore is not None:
        ds_options.append('-i{}'.format(
            args.dpkg_source_diff_ignore))

    for pattern in args.dpkg_source_tar_ignore:
        if pattern is ...:
            ds_options.append('-I')
        else:
            ds_options.append('-I{}'.format(pattern))

    for pattern in args.dpkg_source_extend_diff_ignore:
        ds_options.append('--extend-diff-ignore={}'.format(pattern))

    group = BuildGroup(
        archive_access=args.get_archive_access(),
        binary_version_suffix=_append_to_version or '',
        buildables=(_buildables or '.'),
        components=args.components,
        deb_build_options=deb_build_options,
        dpkg_buildpackage_options=db_options,
        dpkg_source_options=ds_options,
        extra_packages=package_sources_to_debs(extra_packages),
        extra_repositories=_extra_repository,
        extra_repository_keys=_extra_repository_keys,
        extra_test_repositories=extra_test_repository,
        extra_test_repository_keys=extra_test_repository_keys,
        link_builds=args.link_builds,
        orig_dirs=args.orig_dirs,
        output_dir=args.output_dir,
        output_parent=args.output_parent,
        pbuilder_options=_pbuilder_options,
        profiles=profiles,
        sbuild_options=_sbuild_options,
        sbuild_tarball=_sbuild_tarball,
        storage=args.storage,
        suite=args.suite,
        vendor=args.vendor,
    )

    group.select_suites(args)

    for b in group.buildables:
        if build_method == 'pbuilder':
            need_suites = (b.suite, args.pbuilder_worker_suite)
        else:
            need_suites = (b.suite, args.sbuild_worker_suite)

        for suite in need_suites:
            assert isinstance(suite, Suite)

            for ancestor in suite.hierarchy:
                group.archive_access.check_suite(ancestor)

    sbuild_worker = group.get_worker(
        args.sbuild_worker,
        args.sbuild_worker_suite,
    )

    if build_method == 'pbuilder':
        pbuilder_worker = group.get_worker(
            args.pbuilder_worker,
            args.pbuilder_worker_suite,
        )
        group.pbuilder(
            pbuilder_worker,
            archs=_archs,
            indep=_indep,
            indep_together=args.build_indep_together,
        )
    else:
        group.sbuild(
            sbuild_worker,
            archs=_archs,
            build_source=_build_source,
            indep=_indep,
            indep_together=args.build_indep_together,
            source_only=_source_only,
            source_together=args.sbuild_source_together,
        )

    misc_worker = group.get_worker(args.worker, args.worker_suite)

    piuparts_worker = group.get_worker(
        args.piuparts_worker,
        args.piuparts_worker_suite,
    )

    lxc_worker = group.get_worker(
        args.lxc_worker,
        args.lxc_worker_suite,
    )

    lxd_worker = group.get_worker(
        args.lxd_worker,
        args.lxd_worker_suite,
    )

    interrupted = False

    try:
        group.autopkgtest(
            lxc_24bit_subnet=args.lxc_24bit_subnet,
            lxc_worker=lxc_worker,
            lxd_worker=lxd_worker,
            modes=args.autopkgtest,
            qemu_overlay_dir=args.qemu_overlay_dir,
            qemu_ram_size=args.qemu_ram_size,
            schroot_worker=sbuild_worker,
            worker=misc_worker,
        )
    except KeyboardInterrupt:
        interrupted = True

    if args.piuparts_tarballs and not interrupted:
        try:
            group.piuparts(
                ignore=args.piuparts_ignore,
                ignore_regexes=args.piuparts_ignore_regexes,
                tarballs=args.piuparts_tarballs,
                worker=piuparts_worker,
            )
        except KeyboardInterrupt:
            interrupted = True

    _summarize(group.buildables)

    if not interrupted:
        try:
            _lintian(group.buildables)
        except KeyboardInterrupt:
            logger.warning('lintian interrupted')
            interrupted = True

    if _reprepro_dir and not interrupted:
        _publish(group.buildables, _reprepro_dir, _reprepro_suite)

    # We print these separately, right at the end, so that if you built more
    # than one thing, the last screenful of information is the really
    # important bit for testing/signing/upload
    for buildable in group.buildables:
        logger.info(
            'Merged changes files from %s:\n\t%s',
            buildable,
            '\n\t'.join(buildable.merged_changes.values()),
        )

        if buildable.autopkgtest_failures:
            logger.error('Autopkgtest failures for %s:', buildable)
            for x in buildable.autopkgtest_failures:
                logger.error('- %s', x)

        if buildable.piuparts_failures:
            logger.error('Piuparts failures for %s:', buildable)
            for x in buildable.piuparts_failures:
                logger.error('- %s', x)

    for buildable in group.buildables:
        logger.info(
            'Output directory for %s: %s',
            buildable,
            buildable.output_dir,
        )
