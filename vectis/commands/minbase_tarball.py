# Copyright © 2016-2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
from typing import (
    Optional,
)

from debian.debian_support import (
    Version,
)

from vectis.error import ArgumentError
from vectis.worker import (
    VirtWorker,
)

logger = logging.getLogger(__name__)


def run(
    args,
    *,
    _merged_usr: Optional[bool] = None,
    _output=None,
    _uri=None,
):
    if args.suite is None:
        if args.default_suite is not None:
            args.override('suite', args.default_suite)
        else:
            raise ArgumentError('--suite must be specified')

    architecture = args.architecture
    archive_access = args.get_archive_access()
    minbase_tarball = _output
    storage = args.storage
    suite = args.suite
    uri = _uri
    vendor = args.vendor
    worker_argv = args.worker
    worker_suite = args.worker_suite

    apt_key = args.apt_key
    apt_key_package = args.apt_key_package

    os.makedirs(storage, exist_ok=True)

    for suite in (worker_suite, suite):
        for ancestor in suite.hierarchy:
            archive_access.check_suite(ancestor)

    if uri is None:
        uri = archive_access.mirror_for_suite(suite)

    default_stem = 'minbase'

    if suite.merged_usr_required:
        if _merged_usr is False:
            raise ArgumentError(
                f'Suite {suite} does not support non-merged-/usr'
            )

        _merged_usr = True
    elif _merged_usr:
        if not suite.merged_usr_allowed:
            raise ArgumentError(
                f'Suite {suite} did not support merged-/usr'
            )

        default_stem = 'minbase-merged-usr'

    if minbase_tarball is None:
        minbase_tarball = '{}.tar.gz'.format(default_stem)

    if '/' not in minbase_tarball:
        minbase_tarball = '{vendor}/{suite}/{arch}/{tarball}'.format(
            arch=architecture,
            vendor=vendor,
            suite=suite,
            tarball=minbase_tarball,
        )

    logger.info('Creating tarball %s...', minbase_tarball)

    with VirtWorker(
        worker_argv,
        archive_access=archive_access,
        storage=storage,
        suite=worker_suite,
    ) as worker:
        logger.info('Installing debootstrap')
        worker.apt_install([
            'debootstrap',
            'python3',
        ], recommends=False)

        debootstrap_version = worker.dpkg_version('debootstrap')

        if apt_key_package is not None:
            worker.apt_install([apt_key_package], recommends=False)

        debootstrap_args = []

        if worker.call(['test', '-f', apt_key]) == 0:
            logger.info('Found apt key worker:{}'.format(apt_key))
            debootstrap_args.append('--keyring={}'.format(apt_key))
        elif os.path.exists(apt_key):
            logger.info('Found apt key host:{}, copying to worker:{}'.format(
                apt_key, '{}/apt-key.gpg'.format(worker.scratch)))
            worker.copy_to_guest(
                apt_key, '{}/apt-key.gpg'.format(worker.scratch))
            debootstrap_args.append('--keyring={}/apt-key.gpg'.format(
                worker.scratch))
        else:
            logger.warning(
                'Apt key host:{} not found; leaving it out and hoping for the '
                'best'.format(apt_key))

        debootstrap_args.append('--components={}'.format(
            ','.join(args.components)))

        if debootstrap_version >= Version('1.0.86~'):
            if _merged_usr:
                debootstrap_args.append('--merged-usr')
            elif _merged_usr is not None:
                debootstrap_args.append('--no-merged-usr')

        worker.check_call([
            'env', 'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env() + [
            worker.command_wrapper,
            '--',
            'debootstrap',
            '--arch={}'.format(architecture),
            '--components={}'.format(','.join(args.components)),
            '--variant=minbase',
            '--verbose',
        ] + debootstrap_args + [
            str(suite),
            '{}/chroot'.format(worker.scratch),
            uri,
            '/usr/share/debootstrap/scripts/{}'.format(
                args.debootstrap_script),
        ])
        worker.check_call([
            'chroot', '{}/chroot'.format(worker.scratch),
        ] + archive_access.get_proxy_env() + [
            'apt-get', 'clean',
        ])
        worker.check_call([
            'tar', '-C', '{}/chroot'.format(worker.scratch),
            '-f', '{}/output.tar.gz'.format(worker.scratch),
            '-z', '-c', '.',
        ])

        out = os.path.join(storage, minbase_tarball)
        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/output.tar.gz'.format(worker.scratch), out + '.new')
        # FIXME: smoke-test it?
        os.rename(out + '.new', out)

    logger.info('Created tarball %s', minbase_tarball)
