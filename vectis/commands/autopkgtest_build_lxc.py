# Copyright © 2016-2024 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
from typing import TYPE_CHECKING

from vectis.lxc import (
    set_up_lxc_net,
)
from vectis.worker import (
    VirtWorker,
)

if TYPE_CHECKING:
    import vectis.config        # noqa

logger = logging.getLogger(__name__)


def run(
    args: 'vectis.config.Arguments',
) -> None:
    if args.suite is None:
        args.override('suite', args.default_suite)

    architecture = args.architecture
    archive_access = args.get_archive_access()
    storage = args.storage
    suite = args.suite
    assert suite is not None
    vendor = args.vendor
    worker_argv = args.lxc_worker
    worker_suite = args.lxc_worker_suite
    assert worker_suite is not None     # TODO

    apt_key = args.apt_key
    lxc_24bit_subnet = args.lxc_24bit_subnet

    for suite in (worker_suite, suite):
        for ancestor in suite.hierarchy:
            archive_access.check_suite(ancestor)

    os.makedirs(storage, exist_ok=True)

    rootfs_tarball = '{v}/{s}/{a}/lxc-autopkgtest.tar.gz'.format(
        a=architecture,
        s=suite,
        v=vendor,
    )
    meta_tarball = '{v}/{s}/{a}/lxc-autopkgtest-meta.tar.gz'.format(
        a=architecture,
        s=suite,
        v=vendor,
    )
    logger.info('Creating tarballs %s, %s...', rootfs_tarball, meta_tarball)

    with VirtWorker(
        worker_argv,
        archive_access=archive_access,
        storage=storage,
        suite=worker_suite,
    ) as worker:
        logger.info('Installing debootstrap etc.')
        worker.apt_install([
            'autopkgtest',
            'debootstrap',
            'lxc',
            'lxc-templates',
        ])
        set_up_lxc_net(worker, lxc_24bit_subnet)

        argv = [
            'env',
            'AUTOPKGTEST_APT_PROXY={}'.format(
                archive_access.apt_proxy_for_suite(suite)
            ),
            'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env()

        command_wrapper = worker.command_wrapper

        if command_wrapper is not None:
            argv.extend([command_wrapper, '--'])

        argv.extend([
            'autopkgtest-build-lxc',
            '--keyring=' + worker.make_file_available(apt_key),
            str(vendor),
            str(suite),
            architecture,
        ])

        if str(vendor) == 'ubuntu':
            argv.append('--variant=minbase')

        worker.check_call(argv)

        worker.check_call([
            'tar', '-C',
            '/var/lib/lxc/autopkgtest-{}-{}/rootfs'.format(
                suite,
                architecture,
            ),
            '-f', '{}/rootfs.tar.gz'.format(worker.scratch),
            '--exclude=./var/cache/apt/archives/*.deb',
            '-z', '-c', '.',
        ])
        worker.check_call([
            'tar', '-C',
            '/var/lib/lxc/autopkgtest-{}-{}'.format(
                suite,
                architecture,
            ),
            '-f', '{}/meta.tar.gz'.format(worker.scratch),
            '-z', '-c', 'config',
        ])

        out = os.path.join(storage, rootfs_tarball)
        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/rootfs.tar.gz'.format(worker.scratch), out + '.new')
        # FIXME: smoke-test it?
        os.rename(out + '.new', out)

        out = os.path.join(storage, meta_tarball)
        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/meta.tar.gz'.format(worker.scratch), out + '.new')
        # FIXME: smoke-test it?
        os.rename(out + '.new', out)

    logger.info('Created tarballs %s, %s', rootfs_tarball, meta_tarball)
