# Copyright © 2019 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
from typing import (
    Optional,
    Set,
    Tuple,
)

from vectis.config import (
    Arguments,
    Suite,
    Vendor,
)
from vectis.commands.autopkgtest_build_lxc import run as _run_ab_lxc
from vectis.commands.autopkgtest_build_podman import run as _run_ab_podman
from vectis.commands.lxc_tarballs import run as _run_lxc_tarballs
from vectis.commands.minbase_tarball import run as _run_minbase_tarball
from vectis.commands.new import run as _run_new
from vectis.commands.sbuild_tarball import run as _run_sbuild_tarball
from vectis.commands.pbuilder_tarball import run as _run_pbuilder_tarball
from vectis.error import NotSupported


logger = logging.getLogger(__name__)


def _refresh_qemu(
    args: Arguments,
    vendor: Vendor,
    suite: Suite,
    arch: str,
    image: str,
    dry_run: bool = False,
    keep: bool = False,
    merged_usr: Optional[bool] = None,
    done_qemu: Optional[Set[Tuple[str, str, str, str]]] = None,
) -> None:
    if (
        done_qemu is not None
        and (str(vendor), str(suite), arch, image) in done_qemu
    ):
        logger.debug('already generated %s', image)
        return

    logger.info('generating %s', image)
    args = args.copy()
    args.override('vendor', str(vendor))
    args.override('suite', str(suite))
    args.override('architecture', arch)

    if dry_run:
        logger.info('(not really, due to --dry-run)')
    else:
        _run_new(args, _keep=keep, _merged_usr=merged_usr, _output=image)

    if done_qemu is not None:
        done_qemu.add((str(vendor), str(suite), arch, image))


def _refresh(
    args,
    vendor,
    suite,
    arch,
    do_qemu: Optional[bool] = None,
    do_lxc: Optional[bool] = None,
    do_pbuilder: Optional[bool] = None,
    do_sbuild: Optional[bool] = None,
    do_minbase: Optional[bool] = None,
    done_qemu: Optional[Set[Tuple[str, str, str, str]]] = None,
    dry_run: bool = False,
    keep: bool = False,
) -> None:
    do_autopkgtest_lxc: Optional[bool] = do_lxc

    for merged_usr, merged_usr_suffix, allowed in (
        (False, '-no-merged-usr', not suite.merged_usr_required),
        (True, '-merged-usr', suite.merged_usr_allowed),
        (None, '', True),
    ):
        if not allowed:
            continue

        image = '{}/{}/{}/{}/autopkgtest{}.qcow2'.format(
            args.storage, vendor, suite, arch, merged_usr_suffix,
        )

        if do_qemu is None:
            do_this_one = os.path.exists(image)
        else:
            do_this_one = do_qemu

        if (
            done_qemu is not None
            and (str(vendor), str(suite), arch, image) in done_qemu
        ):
            logger.debug('already generated %s', image)
        elif do_this_one:
            _refresh_qemu(
                args,
                vendor,
                suite,
                arch,
                image,
                dry_run=dry_run,
                keep=keep,
                merged_usr=merged_usr,
                done_qemu=done_qemu,
            )
        else:
            logger.debug('not generating %s', image)

    tarball = '{}/{}/{}/{}/sbuild.tar.gz'.format(
        args.storage, vendor, suite, arch,
    )

    if do_sbuild is None:
        do_sbuild = os.path.exists(tarball)

    if do_sbuild:
        logger.info('generating %s', tarball)

        if dry_run:
            logger.info('(not really, due to --dry-run)')
        else:
            _run_sbuild_tarball(args, _keep=keep, _output=tarball)
    else:
        logger.debug('not generating %s', tarball)

    for merged_usr, merged_usr_suffix, allowed in (
        (False, '-no-merged-usr', not suite.merged_usr_required),
        (True, '-merged-usr', suite.merged_usr_allowed),
        (None, '', True),
    ):
        if not allowed:
            continue

        tarball = '{}/{}/{}/{}/minbase{}.tar.gz'.format(
            args.storage, vendor, suite, arch, merged_usr_suffix,
        )

        if do_minbase is None:
            do_this_one = os.path.exists(tarball)
        else:
            do_this_one = do_minbase

        if do_this_one:
            logger.info('generating %s', tarball)

            if dry_run:
                logger.info('(not really, due to --dry-run)')
            else:
                _run_minbase_tarball(
                    args,
                    _merged_usr=merged_usr,
                    _output=tarball,
                )
        else:
            logger.debug('not generating %s', tarball)

    try:
        _run_ab_podman(args)
    except NotSupported as e:
        logger.info('Not generating podman images: %s', e)

    tarball = '{}/{}/{}/{}/pbuilder.tar.gz'.format(
        args.storage, vendor, suite, arch,
    )

    if do_pbuilder is None:
        do_pbuilder = os.path.exists(tarball)

    if do_pbuilder:
        logger.info('generating %s', tarball)

        if dry_run:
            logger.info('(not really, due to --dry-run)')
        else:
            _run_pbuilder_tarball(args, _keep=keep, _output=tarball)
    else:
        logger.debug('not generating %s', tarball)

    tarball = '{}/{}/{}/{}/lxc-rootfs.tar.gz'.format(
        args.storage, vendor, suite, arch,
    )

    if do_lxc is None:
        do_lxc = os.path.exists(tarball)

    if do_lxc:
        logger.info('generating %s', tarball)

        if dry_run:
            logger.info('(not really, due to --dry-run)')
        else:
            _run_lxc_tarballs(args)
    else:
        logger.debug('not generating %s', tarball)

    tarball = '{}/{}/{}/{}/lxc-autopkgtest.tar.gz'.format(
        args.storage, vendor, suite, arch,
    )

    if do_autopkgtest_lxc is None:
        do_autopkgtest_lxc = os.path.exists(tarball)

    if do_autopkgtest_lxc:
        logger.info('generating %s', tarball)

        if dry_run:
            logger.info('(not really, due to --dry-run)')
        else:
            _run_ab_lxc(args)
    else:
        logger.debug('not generating %s', tarball)


def run(
    args,
    *,
    _do_qemu: Optional[bool] = None,
    _do_lxc: Optional[bool] = None,
    _do_sbuild: Optional[bool] = None,
    _do_minbase: Optional[bool] = None,
    _do_pbuilder: Optional[bool] = None,
    _dry_run: bool = False,
    _keep: bool = False,
) -> None:
    done_qemu = set(
    )   # type: Set[Tuple[str, str, str, str]]

    # TODO: Continue if there are errors?
    # TODO: Skip if existing image is sufficiently new

    for suite in args.refresh_suites:
        logger.info('Vendor: %s', suite.vendor)
        logger.info('Suite: %s', suite)
        args2 = args.copy()
        args2.override('vendor', str(suite.vendor))
        args2.override('suite', str(suite))

        for arch in args2.refresh_architectures:
            logger.info('Architecture: %s', arch)
            args3 = args2.copy()
            args3.override('architecture', arch)

            if (
                str(args3.vmdebootstrap_worker_vendor),
                str(args3.vmdebootstrap_worker_suite),
                str(args3.worker_architecture),
                str(args3.vmdebootstrap_worker_qemu_image),
            ) not in done_qemu:
                _refresh_qemu(
                    args,
                    dry_run=_dry_run,
                    keep=_keep,
                    suite=args3.vmdebootstrap_worker_suite,
                    vendor=args3.vmdebootstrap_worker_vendor,
                    arch=args3.worker_architecture,
                    image=args3.vmdebootstrap_worker_qemu_image,
                    done_qemu=done_qemu,
                )

            if (
                str(args3.vmdb2_worker_vendor),
                str(args3.vmdb2_worker_suite),
                str(args3.worker_architecture),
                str(args3.vmdb2_worker_qemu_image),
            ) not in done_qemu:
                _refresh_qemu(
                    args,
                    dry_run=_dry_run,
                    keep=_keep,
                    suite=args3.vmdb2_worker_suite,
                    vendor=args3.vmdb2_worker_vendor,
                    arch=args3.worker_architecture,
                    image=args3.vmdb2_worker_qemu_image,
                    done_qemu=done_qemu,
                )

            _refresh(
                args3, vendor=suite.vendor, suite=suite, arch=arch,
                dry_run=_dry_run, keep=_keep,
                do_qemu=_do_qemu, do_lxc=_do_lxc,
                do_sbuild=_do_sbuild, do_minbase=_do_minbase,
                do_pbuilder=_do_pbuilder,
                done_qemu=done_qemu,
            )
