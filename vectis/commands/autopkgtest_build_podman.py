# Copyright © 2016-2024 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
import shutil
import subprocess
from typing import (
    Optional,
)

from debian.debian_support import (
    Version,
)

from vectis.error import (
    ArgumentError,
    NotSupported,
)

logger = logging.getLogger(__name__)


def run(
    args,
    *,
    _minbase_tarball: Optional[str] = None,
    _uri: Optional[str] = None,
):
    if args.suite is None:
        if args.default_suite is not None:
            args.override('suite', args.default_suite)
        else:
            raise ArgumentError('--suite must be specified')

    architecture = args.architecture
    archive_access = args.get_archive_access()
    storage = args.storage
    suite = args.suite
    vendor = args.vendor

    if shutil.which('podman') is None:
        raise NotSupported('podman not available')

    try:
        output = subprocess.check_output(
            ['dpkg-query', '-W', '-f${Version}', 'autopkgtest'],
            universal_newlines=True,
        ).rstrip('\n')
    except subprocess.CalledProcessError:
        raise NotSupported('dpkg not available')
    else:
        autopkgtest_version = Version(output)

    if autopkgtest_version < Version('5.38'):
        raise NotSupported('autopkgtest-build-podman lacks --tarball option')

    os.makedirs(storage, exist_ok=True)

    if _uri is None:
        uri = archive_access.mirror_for_suite(suite)
    else:
        uri = _uri

    default_stem = 'minbase'

    if _minbase_tarball is None:
        minbase_tarball = f'{default_stem}.tar.gz'
    else:
        minbase_tarball = _minbase_tarball

    if '/' not in minbase_tarball:
        minbase_tarball = f'{vendor}/{suite}/{architecture}/{minbase_tarball}'

    subprocess.run([
        'autopkgtest-build-podman',
        f'--apt-proxy={archive_access.apt_proxy_for_suite(suite)}',
        f'--architecture={architecture}',
        f'--image={architecture}/{vendor}:{suite}',
        '--init=none',
        f'--mirror={uri}',
        f'--release={suite}',
        f'--tarball={storage}/{minbase_tarball}',
        f'--vendor={vendor}',
    ], check=True)

    subprocess.run([
        'autopkgtest-build-podman',
        f'--apt-proxy={archive_access.apt_proxy_for_suite(suite)}',
        f'--architecture={architecture}',
        f'--image={architecture}/{vendor}:{suite}',
        '--init=systemd',
        f'--mirror={uri}',
        f'--release={suite}',
        f'--vendor={vendor}',
    ], check=True)
