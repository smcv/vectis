# Copyright © 2016-2020 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

from vectis.commands.build import run
__all__ = ['run']
