---
# Copyright © 2015-2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

# Default settings for Vectis
defaults:
    vendor: debian
    storage: null
    qemu_overlay_dir: null
    qemu_ram_size: 1G
    qemu_image_size: 32GiB
    components: main
    extra_components: []
    http_proxy: null
    https_proxy: null
    ftp_proxy: null
    no_proxy: []
    mirrors:
        null: null
        # Examples, in descending order of priority:
        # By full URI of a canonical mirror:
        #   http://deb.debian.org/debian: http://mirror/debian
        # By 'archive' property of a suite:
        #   security.debian.org: http://mirror/debian-security
        # Default for everything not otherwise matched:
        #   null: http://192.168.122.1:3142/${archive}
    uris: []
    qemu_image: autopkgtest.qcow2
    write_qemu_image: null
    debootstrap_script: null
    apt_key_package: null
    apt_key: null
    default_suite: null
    default_worker_suite: null
    architecture: null
    suite: null
    kernel_package: null

    worker_vendor: debian
    worker_suite: null
    worker_architecture: null
    worker: null
    worker_qemu_image: null

    lxc_24bit_subnet: '10.0.3'
    lxc_worker_qemu_image: null
    lxc_worker_suite: null
    lxc_worker_vendor: null
    lxc_worker_architecture: null
    lxc_worker: null

    lxd_worker_qemu_image: apparmor.qcow2
    lxd_worker_suite: null
    lxd_worker_vendor: ubuntu
    lxd_worker_architecture: null
    lxd_worker: null

    sbuild_worker_qemu_image: null
    sbuild_worker_suite: null
    sbuild_worker_vendor: null
    sbuild_worker_architecture: null
    sbuild_worker: null

    pbuilder_worker_qemu_image: null
    pbuilder_worker_suite: null
    pbuilder_worker_vendor: null
    pbuilder_worker_architecture: null
    pbuilder_worker: null

    piuparts_worker_qemu_image: null
    piuparts_worker_suite: null
    piuparts_worker_vendor: null
    piuparts_worker_architecture: null
    piuparts_worker: null

    vmdebootstrap_worker_suite: null
    vmdebootstrap_worker_qemu_image: null
    vmdebootstrap_worker_vendor: null
    vmdebootstrap_worker: null
    vmdebootstrap_worker_architecture: null
    vmdebootstrap_options: []

    vmdb2_worker_suite: null
    vmdb2_worker_qemu_image: null
    vmdb2_worker_vendor: null
    vmdb2_worker: null
    vmdb2_worker_architecture: null

    autopkgtest:
        - schroot
        - qemu:autopkgtest.qcow2

    piuparts_tarballs:
        - minbase.tar.gz
    piuparts_ignore: []
    piuparts_ignore_regexes: []

    parallel: null
    build_indep_together: false
    sbuild_source_together: false
    orig_dirs: [".."]
    output_dir: null
    output_parent: ".."
    link_builds: [".."]

    sbuild_buildables: null
    sbuild_resolver: []
    cross_sbuild_resolver: []
    dpkg_source_tar_ignore: []
    dpkg_source_diff_ignore: null
    dpkg_source_extend_diff_ignore: []

    refresh_vendors: []
    refresh_suites: []
    refresh_architectures: []

    merged_usr_allowed: true
    merged_usr_required: true

# Built-in knowledge about well-known Debian derivatives.
#
# Please follow these principles when modifying:
#
# - If old and new suites differ, the vendor-wide default should be the
#   one that works for new suites.
# - Don't enable non-free software unless a typical user of that vendor
#   would always enable it.

vendors:
    debian:
        autopkgtest:
            - lxc
            - qemu:autopkgtest.qcow2
        extra_components: contrib non-free non-free-firmware
        worker_vendor: debian
        apt_key_package: debian-archive-keyring
        apt_key: /usr/share/keyrings/debian-archive-keyring.gpg
        vmdebootstrap_options:
            - "--package=libnss-systemd"
        lxc_worker_suite: bookworm-backports
        uris:
            - http://deb.debian.org/debian
            - http://ftp.debian.org/debian
            - ftp://ftp.debian.org/debian
        kernel_package:
            i386: linux-image-686
        suites:
            jessie:
                extra_components: contrib non-free
                vmdebootstrap_options: []
                kernel_package:
                    i386: linux-image-586
                piuparts_tarballs:
                    - minbase.tar.gz
                autopkgtest:
                    - lxc
                    - qemu:autopkgtest.qcow2
                merged_usr_allowed: false
                merged_usr_required: false

            stretch:
                extra_components: contrib non-free
                merged_usr_required: false
            buster:
                extra_components: contrib non-free
                merged_usr_required: false
            bullseye:
                extra_components: contrib non-free
                merged_usr_required: false

            sid: {}
            unstable:
                alias_for: sid

            experimental:
                base: sid
                sbuild_resolver:
                    - "--build-dep-resolver=aspcud"
                    - "--aspcud-criteria=-count(solution,APT-Release:=/experimental/),-removed,-changed,-new"
                cross_sbuild_resolver:
                    - "--build-dep-resolver=aptitude"
            rc-buggy:
                alias_for: experimental

            "buster-backports":
                base: buster
                extra_components: contrib non-free
                needs_pinning: true
                sbuild_resolver:
                    - "--build-dep-resolver=aptitude"
                merged_usr_required: false
            "bullseye-backports":
                base: bullseye
                extra_components: contrib non-free
                needs_pinning: true
                sbuild_resolver:
                    - "--build-dep-resolver=aptitude"
                merged_usr_required: false
            "*-backports":              # >= trixie-backports
                needs_pinning: true
                sbuild_resolver:
                    - "--build-dep-resolver=aptitude"

            "buster-backports-sloppy":
                base: buster
                extra_components: contrib non-free
                needs_pinning: true
                sbuild_resolver:
                    - "--build-dep-resolver=aptitude"
                merged_usr_required: false
            "bullseye-backports-sloppy":
                base: bullseye
                extra_components: contrib non-free
                needs_pinning: true
                sbuild_resolver:
                    - "--build-dep-resolver=aptitude"
                merged_usr_required: false
            "*-backports-sloppy":       # >= bookworm-backports-sloppy
                needs_pinning: true
                base: '*-backports'
                sbuild_resolver:
                    - "--build-dep-resolver=aptitude"

            # *-proposed-updates intentionally omitted because nobody is
            # meant to upload to it

            "jessie-security":
                base: jessie
                extra_components: contrib non-free
                apt_suite: "jessie/updates"
                archive: "debian-security"
                uris:
                    - http://security.debian.org/debian-security
                merged_usr_allowed: false
                merged_usr_required: false
            "stretch-security":
                base: stretch
                extra_components: contrib non-free
                apt_suite: "stretch/updates"
                archive: "debian-security"
                uris:
                    - http://security.debian.org/debian-security
                merged_usr_required: false
            "buster-security":
                base: buster
                extra_components: contrib non-free
                apt_suite: "buster/updates"
                archive: "debian-security"
                uris:
                    - http://security.debian.org/debian-security
                merged_usr_required: false
            "bullseye-security":
                base: bullseye
                extra_components: contrib non-free
                archive: "debian-security"
                uris:
                    - http://security.debian.org/debian-security
                merged_usr_required: false
            "*-security":   # >= bookworm-security
                archive: "debian-security"
                uris:
                    - http://security.debian.org/debian-security

            "jessie-updates":
                extra_components: contrib non-free
                merged_usr_allowed: false
                merged_usr_required: false
            "stretch-updates":
                extra_components: contrib non-free
                merged_usr_required: false
            "buster-updates":
                extra_components: contrib non-free
                merged_usr_required: false
            "bullseye-updates":
                extra_components: contrib non-free
                merged_usr_required: false
            "*-updates":    # >= bookworm-updates
                null: null
    ubuntu:
        apt_key_package: ubuntu-keyring
        apt_key: /usr/share/keyrings/ubuntu-archive-keyring.gpg
        autopkgtest:
            - lxc
            - qemu
        worker_vendor: ubuntu
        components: main universe
        extra_components: restricted multiverse
        kernel_package:
            null: linux-image-generic
        vmdebootstrap_options:
            - "--package=libnss-systemd"
        uris:
            - http://archive.ubuntu.com/ubuntu
        suites:
            "*-backports":
                needs_pinning: true
            "*-proposed":
                null: null
            "*-security":
                null: null
            "*-updates":
                null: null

directories:
    /:
        # Directory-specific configuration has highest priority, do not put
        # anything in here by default. We configure '/' so that path search
        # always terminates.
        null: null

# vim:set sw=4 sts=4 et:
