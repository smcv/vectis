# Copyright © 2016 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import contextlib
import logging
import os
from typing import (
    Any,
    Iterator,
    TextIO,
)

logger = logging.getLogger(__name__)


@contextlib.contextmanager
def AtomicWriter(fn: str, *a: Any, **k: Any) -> Iterator[TextIO]:
    try:
        with open(fn + '.tmp', 'x', *a, **k) as f:
            yield f
    except Exception:
        try:
            os.unlink(fn + '.tmp')
        except Exception as e:
            logger.warning('Could not unlink "%s.tmp": %s', fn, e)
        raise
    else:
        os.rename(fn + '.tmp', fn)


def parse_byte_size(value: str) -> int:
    """
    Convert a byte size in IEC, SI or traditional units to a number of
    bytes.
    """

    if value.endswith('T'):
        return int(value[:-1]) * 1024 * 1024 * 1024 * 1024
    elif value.endswith('TiB'):
        return int(value[:-3]) * 1024 * 1024 * 1024 * 1024
    elif value.endswith('TB'):
        return int(value[:-2]) * 1000 * 1000 * 1000 * 1000
    elif value.endswith('G'):
        return int(value[:-1]) * 1024 * 1024 * 1024
    elif value.endswith('GiB'):
        return int(value[:-3]) * 1024 * 1024 * 1024
    elif value.endswith('GB'):
        return int(value[:-2]) * 1000 * 1000 * 1000
    elif value.endswith('M'):
        return int(value[:-1]) * 1024 * 1024
    elif value.endswith('MiB'):
        return int(value[:-3]) * 1024 * 1024
    elif value.endswith('MB'):
        return int(value[:-2]) * 1000 * 1000
    elif value.endswith('K'):
        return int(value[:-1]) * 1024
    elif value.endswith('KiB'):
        return int(value[:-3]) * 1024
    elif value.endswith('kB'):
        return int(value[:-2]) * 1000
    else:
        return int(value)


def format_byte_size_iec(value: int) -> str:
    if value % (1024 * 1024 * 1024 * 1024) == 0:
        return '%dTiB' % (value / 1024 / 1024 / 1024 / 1024)
    elif value % (1024 * 1024 * 1024) == 0:
        return '%dGiB' % (value / 1024 / 1024 / 1024)
    elif value % (1024 * 1024) == 0:
        return '%dMiB' % (value / 1024 / 1024)
    elif value % (1024) == 0:
        return '%dKiB' % (value / 1024)
    else:
        return '%d' % value


def format_byte_size_traditional(value: int) -> str:
    if value % (1024 * 1024 * 1024 * 1024) == 0:
        return '%dT' % (value / 1024 / 1024 / 1024 / 1024)
    elif value % (1024 * 1024 * 1024) == 0:
        return '%dG' % (value / 1024 / 1024 / 1024)
    elif value % (1024 * 1024) == 0:
        return '%dM' % (value / 1024 / 1024)
    elif value % (1024) == 0:
        return '%dK' % (value / 1024)
    else:
        return '%d' % value


def format_byte_size_si(value: int) -> str:
    if value % (1000 * 1000 * 1000 * 1000) == 0:
        return '%dTB' % (value / 1000 / 1000 / 1000 / 1000)
    elif value % (1000 * 1000 * 1000) == 0:
        return '%dGB' % (value / 1000 / 1000 / 1000)
    elif value % (1000 * 1000) == 0:
        return '%dMB' % (value / 1000 / 1000)
    elif value % (1000) == 0:
        return '%dkB' % (value / 1000)
    else:
        return '%d' % value
