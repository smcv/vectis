#!/usr/bin/python3

# Copyright © 2020 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import unittest

from vectis.util import (
    format_byte_size_iec,
    format_byte_size_si,
    format_byte_size_traditional,
    parse_byte_size,
)


TiB = 1024 * 1024 * 1024 * 1024
TB = 1000 * 1000 * 1000 * 1000
GiB = 1024 * 1024 * 1024
GB = 1000 * 1000 * 1000
MiB = 1024 * 1024
MB = 1000 * 1000
KiB = 1024
kB = 1000


class TestCase(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_parse_byte_size(self) -> None:
        parse = parse_byte_size

        for n in (1, 3, 23, 1234567890):
            self.assertEqual(parse('%d' % n), n)

            self.assertEqual(parse('%dGB' % n), n * GB)
            self.assertEqual(parse('%dG' % n), n * GiB)
            self.assertEqual(parse('%dGiB' % n), n * GiB)

            self.assertEqual(parse('%dMB' % n), n * MB)
            self.assertEqual(parse('%dM' % n), n * MiB)
            self.assertEqual(parse('%dMiB' % n), n * MiB)

            self.assertEqual(parse('%dkB' % n), n * kB)
            self.assertEqual(parse('%dK' % n), n * KiB)
            self.assertEqual(parse('%dKiB' % n), n * KiB)

    def test_format_byte_size_iec(self) -> None:
        f = format_byte_size_iec
        t = format_byte_size_traditional

        for n in (1, 3, 23, 1234567890):
            self.assertEqual(f(n), '%d' % n)
            self.assertEqual(t(n), '%d' % n)

            self.assertEqual(f(n * TiB), '%dTiB' % n)
            self.assertEqual(t(n * TiB), '%dT' % n)

            self.assertEqual(f(n * MiB), '%dMiB' % n)
            self.assertEqual(t(n * MiB), '%dM' % n)

            self.assertEqual(f(n * KiB), '%dKiB' % n)
            self.assertEqual(t(n * KiB), '%dK' % n)

    def test_format_byte_size_si(self) -> None:
        f = format_byte_size_si

        for n in (1, 3, 23, 1234567890):
            self.assertEqual(f(n), '%d' % n)
            self.assertEqual(f(n * TB), '%dTB' % n)
            self.assertEqual(f(n * MB), '%dMB' % n)
            self.assertEqual(f(n * kB), '%dkB' % n)

    def tearDown(self) -> None:
        pass


if __name__ == '__main__':
    import tap
    runner = tap.TAPTestRunner()
    runner.set_stream(True)
    unittest.main(verbosity=2, testRunner=runner)
