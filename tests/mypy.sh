#!/bin/sh
# Copyright © 2016-2018 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

set -eu

cd "${VECTIS_UNINSTALLED:-.}"

export MYPYPATH="${PYTHONPATH-.}"

if [ "${MYPY:=mypy}" = false ]; then
    echo "1..0 # SKIP mypy not found"
elif "${MYPY}" \
        --python-executable="${PYTHON:=python3}" \
        --ignore-missing-imports \
        --follow-imports=skip \
        -p vectis -p tests >&2; then
    echo "1..1"
    echo "ok 1 - mypy reported no issues"
else
    echo "1..1"
    echo "not ok 1 # TODO mypy issues reported"
fi

# vim:set sw=4 sts=4 et:
