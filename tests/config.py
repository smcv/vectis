#!/usr/bin/python3

# Copyright © 2016 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

import io
import os
import subprocess
import unittest

from typing import (
    Optional,
)

import yaml

from vectis.config import (
    Arguments,
    Config,
    Suite,
    Vendor,
)

XDG_CACHE_HOME = os.getenv('XDG_CACHE_HOME', os.path.expanduser('~/.cache'))

try:
    ARCHITECTURE: Optional[str] = subprocess.check_output(
        ['dpkg', '--print-architecture'],
        universal_newlines=True
    ).strip()
except Exception:
    ARCHITECTURE = None

CONFIG = """
defaults:
    mirrors:
        null: http://192.168.122.1:3142/${archive}
        steamos: http://localhost/steamos
        http://archive.ubuntu.com/ubuntu: https://mirror/ubuntu
    http_proxy: http://proxy:8080
    https_proxy: http://sproxy:8080
    ftp_proxy: http://fproxy:8080
    no_proxy:
        - 192.168.122.1
    architecture: mips
    refresh_vendors:
        - debian
        - steamrt
        - tanglu
"""

VENDORS = """
vendors:
    steamrt:
        archive: repo.steamstatic.com/steamrt
        uris:
            - http://repo.steamstatic.com/steamrt
        components:
            - main
            - contrib
            - non-free
        refresh_suites:
            - scout
        suites:
            scout:
                base: ubuntu/precise
"""

GiB = 1024 * 1024 * 1024


class DefaultsTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.__config = Config(
            config_layers=(
                yaml.safe_load(io.StringIO(CONFIG)),
                yaml.safe_load(io.StringIO(VENDORS)),
            ),
            current_directory='/',
        )

    def test_defaults(self) -> None:
        self.__config = Config(
            config_layers=[{}],
            current_directory='/',
        )
        c = Arguments('test', self.__config)

        self.assertGreaterEqual(c.parallel, 1)
        self.assertIs(type(c.parallel), int)

        debian = c.get_vendor('debian')
        ubuntu = c.get_vendor('ubuntu')
        self.assertIsInstance(debian, Vendor)
        self.assertIsInstance(ubuntu, Vendor)

        self.assertEqual(str(c.vendor), 'debian')
        self.assertEqual(str(c.worker_vendor), 'debian')
        self.assertEqual(
            str(c.vmdebootstrap_worker_vendor),
            'debian',
        )
        self.assertEqual(str(c.sbuild_worker_vendor), 'debian')
        self.assertEqual(str(c.pbuilder_worker_vendor), 'debian')
        self.assertIs(c.vendor, debian)
        self.assertIs(c.worker_vendor, debian)
        self.assertIs(c.pbuilder_worker_vendor, debian)
        self.assertIs(c.sbuild_worker_vendor, debian)
        self.assertIs(c.vmdebootstrap_worker_vendor, debian)

        with self.assertRaises(AttributeError):
            c.archive       # type: ignore

        self.assertIs(c.build_indep_together, False)
        self.assertIs(c.sbuild_source_together, False)
        self.assertEqual(c.output_parent, '..')
        self.assertEqual(c.qemu_image_size, 32 * GiB)
        self.assertIsNone(c.sbuild_buildables)
        self.assertEqual(c.sbuild_resolver, [])
        self.assertEqual(c.debootstrap_script, None)
        self.assertEqual(
            c.apt_key,
            '/usr/share/keyrings/debian-archive-keyring.gpg',
        )
        self.assertEqual(c.dpkg_source_tar_ignore, [])
        self.assertIsNone(c.dpkg_source_diff_ignore)
        self.assertEqual(c.dpkg_source_extend_diff_ignore, [])

        if ARCHITECTURE is not None:
            self.assertEqual(c.architecture, ARCHITECTURE)

        if 0:
            # FIXME: these raise, because suite is undefined,
            # but then the error is trapped and __getattr__ is called
            # instead
            self.assertEqual(
                c.qemu_image,
                '{}/vectis/{}/debian/sid/autopkgtest.qcow2'.format(
                    XDG_CACHE_HOME, ARCHITECTURE)
            )
            self.assertEqual(c.worker_architecture, ARCHITECTURE)
            self.assertEqual(
                c.worker,
                ['qemu', c.worker_qemu_image])
            self.assertEqual(
                c.worker_qemu_image,
                '{}/vectis/{}/debian/sid/autopkgtest.qcow2'.format(
                    XDG_CACHE_HOME, ARCHITECTURE))
            self.assertEqual(
                c.pbuilder_worker,
                ['qemu', c.pbuilder_worker_qemu_image])
            self.assertEqual(
                c.pbuilder_worker_qemu_image,
                '{}/vectis/{}/debian/sid/autopkgtest.qcow2'.format(
                    XDG_CACHE_HOME, ARCHITECTURE))
            self.assertEqual(
                c.sbuild_worker,
                ['qemu', c.sbuild_worker_qemu_image])
            self.assertEqual(
                c.sbuild_worker_qemu_image,
                '{}/vectis/{}/debian/sid/autopkgtest.qcow2'.format(
                    XDG_CACHE_HOME, ARCHITECTURE))

        self.assertEqual(c.autopkgtest, [
            'lxc', 'qemu:autopkgtest.qcow2',
        ])
        self.assertEqual(c.suite, None)
        with self.assertRaises(AttributeError):
            c.apt_suite     # type: ignore

        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            pass
        else:
            self.assertEqual(
                c.worker_suite,
                c.ensure_suite(
                    c.vendor,
                    distro_info.DebianDistroInfo().stable()))
            self.assertEqual(
                c.default_worker_suite,
                distro_info.DebianDistroInfo().stable())

        stable = c.ensure_suite(c.vendor, 'stable')
        self.assertEqual(c.pbuilder_worker_suite, stable)
        self.assertEqual(c.sbuild_worker_suite, stable)
        self.assertEqual(c.piuparts_worker_suite, stable)
        self.assertEqual(c.default_suite, 'sid')
        self.assertEqual(c.components, {'main'})
        self.assertEqual(
            c.extra_components,
            {'contrib', 'non-free', 'non-free-firmware'},
        )
        self.assertEqual(
            c.all_components,
            {'main', 'contrib', 'non-free', 'non-free-firmware'},
        )

        self.assertEqual(
            c.storage,
            '{}/vectis'.format(XDG_CACHE_HOME))

    def test_refresh_suites(self) -> None:
        c = Arguments('test', self.__config)
        c.override('refresh_suites', ['debian/stretch', 'steamos/brewmaster'])
        self.assertEqual(
            set(['{}/{}'.format(s.vendor, s) for s in c.refresh_suites]),
            set(['debian/stretch', 'steamos/brewmaster']),
        )

        c = Arguments('test', self.__config)
        c.override('refresh_vendors', ['steamrt'])
        self.assertEqual(
            set(['{}/{}'.format(s.vendor, s) for s in c.refresh_suites]),
            set(['steamrt/scout']),
        )

        c = Arguments('test', self.__config)
        c.override('refresh_vendors', ['tanglu'])
        self.assertEqual(
            set(['{}/{}'.format(s.vendor, s) for s in c.refresh_suites]),
            set([]),
        )

        c = Arguments('test', self.__config)
        c.override('refresh_vendors', [])
        self.assertNotEqual(
            set(['{}/{}'.format(s.vendor, s) for s in c.refresh_suites]),
            set([]),
        )

    def test_substitutions(self) -> None:
        c = Arguments('test', self.__config)

        c.override('architecture', 'm68k')
        c.override('suite', 'potato')
        c.override('worker_suite', 'sarge')
        c.override('qemu_ram_size', '512M')
        c.override('parallel', 1)
        c.override('pbuilder_worker_suite', 'chromodoris')
        c.override('pbuilder_worker_vendor', 'tanglu')
        c.override('sbuild_worker_suite', 'alchemist')
        c.override('sbuild_worker_vendor', 'steamos')
        c.override('piuparts_worker_suite', 'scout')
        c.override('piuparts_worker_vendor', 'steamrt')
        c.override('vmdebootstrap_worker_suite', 'xenial')
        c.override('vmdebootstrap_worker_vendor', 'ubuntu')

        debian = c.get_vendor('debian')
        potato = c.ensure_suite(debian, 'potato')
        sarge = c.ensure_suite(debian, 'sarge')
        self.assertEqual(list(potato.hierarchy), [potato])
        self.assertEqual(list(sarge.hierarchy), [sarge])
        self.assertEqual(c.suite, potato)
        self.assertEqual(c.worker_suite, sarge)

        self.assertEqual(c.debootstrap_script, 'potato')
        self.assertEqual(
            c.qemu_image,
            '{}/debian/potato/m68k/autopkgtest.qcow2'.format(
                c.storage))
        self.assertEqual(
            c.worker_qemu_image,
            '{}/debian/sarge/m68k/autopkgtest.qcow2'.format(
                c.storage))
        self.assertEqual(
            c.worker, [
                'qemu', '--ram-size=512', '--cpus=1',
                '{}/debian/sarge/m68k/autopkgtest.qcow2'.format(
                    c.storage)
            ]
        )

        self.assertEqual(
            c.pbuilder_worker_qemu_image,
            '{}/tanglu/chromodoris/m68k/autopkgtest.qcow2'.format(
                    c.storage))
        self.assertEqual(
            c.pbuilder_worker, [
                'qemu', '--ram-size=512', '--cpus=1',
                '{}/tanglu/chromodoris/m68k/autopkgtest.qcow2'.format(
                    c.storage)])
        self.assertEqual(
            c.piuparts_worker_qemu_image,
            '{}/steamrt/precise/m68k/autopkgtest.qcow2'.format(
                c.storage))
        self.assertEqual(
            c.piuparts_worker, [
                'qemu', '--ram-size=512', '--cpus=1',
                '{}/steamrt/precise/m68k/autopkgtest.qcow2'.format(
                    c.storage)])
        self.assertEqual(
            c.sbuild_worker_qemu_image,
            '{}/steamos/alchemist/m68k/autopkgtest.qcow2'.format(
                c.storage))
        self.assertEqual(
            c.sbuild_worker, [
                'qemu', '--ram-size=512', '--cpus=1',
                '{}/steamos/alchemist/m68k/autopkgtest.qcow2'.format(
                    c.storage)])
        self.assertEqual(
            c.vmdebootstrap_worker_qemu_image,
            '{}/ubuntu/xenial/m68k/autopkgtest.qcow2'.format(
                c.storage))
        self.assertEqual(
            c.vmdebootstrap_worker, [
                'qemu', '--ram-size=512', '--cpus=1',
                '{}/ubuntu/xenial/m68k/autopkgtest.qcow2'.format(
                    c.storage)])

        self.assertEqual(
            c.get_archive_access().mirror_for_suite(potato),
            'http://192.168.122.1:3142/debian')
        self.assertEqual(
            c.get_archive_access().apt_proxy_for_suite(potato),
            'DIRECT')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(sarge),
            'http://192.168.122.1:3142/debian')

        with self.assertRaises(AttributeError):
            c.apt_suite     # type: ignore

    def test_debian(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'debian')
        c.override('refresh_vendors', ['debian'])
        c.override('suite', 'sid')

        debian = c.get_vendor('debian')
        self.assertIs(c.vendor, debian)

        sid = c.ensure_suite(debian, 'sid')

        self.assertIs(c.suite, sid)

        # Properties of the vendor itself
        self.assertEqual(str(debian), 'debian')
        self.assertEqual(debian.default_suite, 'sid')
        self.assertIs(c.ensure_suite(debian, 'unstable'), sid)
        self.assertEqual(debian.components, {'main'})
        self.assertEqual(
            debian.extra_components,
            {'contrib', 'non-free', 'non-free-firmware'},
        )
        self.assertEqual(
            debian.all_components,
            {'main', 'contrib', 'non-free', 'non-free-firmware'},
        )
        self.assertIsNone(c.get_existing_suite(debian, 'xenial'))

        # Properties of the suite itswelf
        self.assertEqual(
            sid.apt_key,
            '/usr/share/keyrings/debian-archive-keyring.gpg')
        self.assertEqual(sid.archive, 'debian')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(sid),
            'http://192.168.122.1:3142/debian')
        self.assertIs(sid.base, None)
        self.assertEqual(sid.components, {'main'})
        self.assertEqual(sid.extra_components, debian.extra_components)
        self.assertEqual(sid.all_components, debian.all_components)
        self.assertEqual(sid.apt_suite, 'sid')
        self.assertEqual(sid.sbuild_resolver, [])

        # Properties of the Arguments determined by the suite being Debian sid
        self.assertEqual(c.autopkgtest, [
            'lxc', 'qemu:autopkgtest.qcow2',
        ])
        self.assertIs(c.worker_vendor, debian)
        self.assertIs(c.lxc_worker_vendor, debian)
        self.assertIs(c.pbuilder_worker_vendor, debian)
        self.assertIs(c.piuparts_worker_vendor, debian)
        self.assertIs(c.sbuild_worker_vendor, debian)
        self.assertIs(c.vmdebootstrap_worker_vendor, debian)
        with self.assertRaises(AttributeError):
            c.archive       # type: ignore
        self.assertEqual(c.qemu_image_size, 32 * GiB)
        self.assertGreaterEqual(c.parallel, 1)
        self.assertIs(c.build_indep_together, False)
        self.assertIs(c.sbuild_source_together, False)
        self.assertEqual(c.sbuild_resolver, [])
        self.assertEqual(
            c.apt_key,
            '/usr/share/keyrings/debian-archive-keyring.gpg')
        self.assertIsNone(c.dpkg_source_diff_ignore)
        self.assertEqual(c.dpkg_source_tar_ignore, [])
        self.assertEqual(c.dpkg_source_extend_diff_ignore, [])
        self.assertEqual(c.output_parent, '..')
        self.assertEqual(c.architecture, 'mips')
        self.assertEqual(c.worker_architecture, 'mips')
        self.assertEqual(c.lxc_worker_architecture, 'mips')
        self.assertEqual(c.pbuilder_worker_architecture, 'mips')
        self.assertEqual(c.piuparts_worker_architecture, 'mips')
        self.assertEqual(c.sbuild_worker_architecture, 'mips')

        with self.assertRaises(AttributeError):
            c.apt_suite     # type: ignore

        # Below this point relies on knowledge of distro_info
        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        debian_info = distro_info.DebianDistroInfo()
        self.assertEqual(
            debian.default_worker_suite,
            debian_info.stable())
        self.assertIs(
            c.lxc_worker_suite,
            c.ensure_suite(debian, 'bookworm-backports'))
        self.assertIs(
            c.pbuilder_worker_suite,
            c.ensure_suite(debian, debian_info.stable()))
        self.assertIs(
            c.piuparts_worker_suite,
            c.ensure_suite(debian, debian_info.stable()))
        self.assertIs(
            c.sbuild_worker_suite,
            c.ensure_suite(debian, debian_info.stable()))
        self.assertIs(
            c.vmdebootstrap_worker_suite,
            c.ensure_suite(debian, debian_info.stable()))
        self.assertIs(
            c.worker_suite,
            c.ensure_suite(debian, debian_info.stable()))
        self.assertEqual(
            [str(x) for x in c.refresh_suites],
            ['sid', debian_info.testing(), debian_info.stable()])

        self.assertEqual(
            str(c.ensure_suite(debian, 'unstable')),
            'sid')
        self.assertEqual(
            str(c.ensure_suite(debian, 'testing')),
            debian_info.testing())
        self.assertEqual(
            str(c.ensure_suite(debian, 'oldstable')),
            debian_info.old())
        self.assertEqual(
            str(c.ensure_suite(debian, 'rc-buggy')),
            'experimental')
        stable = c.ensure_suite(debian, 'stable')
        self.assertEqual(str(stable), debian_info.stable())

    def test_debian_bullseye(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'debian')
        c.override('refresh_vendors', ['debian'])
        c.override('suite', 'bullseye')

        debian = c.get_vendor('debian')
        bullseye = c.ensure_suite(debian, 'bullseye')
        self.assertIs(c.suite, bullseye)

        # Properties of the suite itswelf
        self.assertEqual(
            bullseye.apt_key,
            '/usr/share/keyrings/debian-archive-keyring.gpg')
        self.assertEqual(bullseye.archive, 'debian')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(bullseye),
            'http://192.168.122.1:3142/debian')
        self.assertIs(bullseye.base, None)
        self.assertEqual(bullseye.components, {'main'})
        self.assertEqual(
            bullseye.extra_components,
            debian.extra_components - {'non-free-firmware'},
        )
        self.assertEqual(
            bullseye.all_components,
            debian.all_components - {'non-free-firmware'},
        )
        self.assertEqual(bullseye.apt_suite, 'bullseye')
        self.assertEqual(bullseye.sbuild_resolver, [])

        # Properties of the Arguments determined by the suite being Debian
        # bullseye
        self.assertEqual(c.autopkgtest, [
            'lxc', 'qemu:autopkgtest.qcow2',
        ])
        self.assertIs(c.worker_vendor, debian)
        self.assertIs(c.lxc_worker_vendor, debian)
        self.assertIs(c.pbuilder_worker_vendor, debian)
        self.assertIs(c.piuparts_worker_vendor, debian)
        self.assertIs(c.sbuild_worker_vendor, debian)
        self.assertIs(c.vmdebootstrap_worker_vendor, debian)
        with self.assertRaises(AttributeError):
            c.archive       # type: ignore
        self.assertEqual(c.qemu_image_size, 32 * GiB)
        self.assertGreaterEqual(c.parallel, 1)
        self.assertIs(c.build_indep_together, False)
        self.assertIs(c.sbuild_source_together, False)
        self.assertEqual(c.sbuild_resolver, [])
        self.assertEqual(
            c.apt_key,
            '/usr/share/keyrings/debian-archive-keyring.gpg')
        self.assertIsNone(c.dpkg_source_diff_ignore)
        self.assertEqual(c.dpkg_source_tar_ignore, [])
        self.assertEqual(c.dpkg_source_extend_diff_ignore, [])
        self.assertEqual(c.output_parent, '..')
        self.assertEqual(c.architecture, 'mips')
        self.assertEqual(c.worker_architecture, 'mips')
        self.assertEqual(c.lxc_worker_architecture, 'mips')
        self.assertEqual(c.pbuilder_worker_architecture, 'mips')
        self.assertEqual(c.piuparts_worker_architecture, 'mips')
        self.assertEqual(c.sbuild_worker_architecture, 'mips')

        with self.assertRaises(AttributeError):
            c.apt_suite     # type: ignore

    def test_debian_experimental(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'debian')
        c.override('suite', 'experimental')

        debian = c.get_vendor('debian')
        self.assertIs(c.vendor, debian)

        experimental = c.ensure_suite(debian, 'experimental')
        self.assertIs(c.ensure_suite(debian, 'rc-buggy'), experimental)
        self.assertIs(c.suite, experimental)

        # Properties of the suite itself
        self.assertEqual(
            list(experimental.hierarchy),
            [experimental, c.ensure_suite(debian, 'sid')])
        self.assertIs(
            experimental.base, c.ensure_suite(debian, 'sid'))
        self.assertEqual(
            experimental.sbuild_resolver[0], '--build-dep-resolver=aspcud',
        )

        # Properties of the Config determined by the suite being
        # Debian experimental
        self.assertEqual(c.sbuild_resolver[0], '--build-dep-resolver=aspcud')

    def test_debian_backports(self) -> None:
        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        c = Arguments('test', self.__config)
        c.override('vendor', 'debian')
        c.override('suite', 'stable-backports')

        debian = c.get_vendor('debian')

        self.assertIs(c.vendor, debian)

        debian_info = distro_info.DebianDistroInfo()
        backports = c.ensure_suite(debian, 'stable-backports')
        self.assertIsInstance(backports, Suite)
        stable = c.ensure_suite(debian, 'stable')
        self.assertIs(c.suite, backports)
        self.assertEqual(
            str(backports),
            debian_info.stable() + '-backports')
        self.assertEqual(backports.hierarchy[0], backports)
        self.assertEqual(str(backports.hierarchy[1]), str(stable))
        self.assertEqual(
            backports.sbuild_resolver,
            ['--build-dep-resolver=aptitude'])
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(backports),
            'http://192.168.122.1:3142/debian')
        self.assertEqual(backports.archive, 'debian')

        self.assertEqual(
            c.sbuild_resolver,
            ['--build-dep-resolver=aptitude'])

    def test_debian_backports_sloppy(self) -> None:
        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        c = Arguments('test', self.__config)
        c.override('vendor', 'debian')
        c.override('suite', 'stable-backports-sloppy')

        debian = c.get_vendor('debian')
        self.assertIs(c.vendor, debian)
        debian_info = distro_info.DebianDistroInfo()

        sloppy = c.ensure_suite(debian, 'stable-backports-sloppy')
        backports = c.ensure_suite(debian, 'stable-backports')
        self.assertIsInstance(backports, Suite)
        stable = c.ensure_suite(debian, 'stable')
        self.assertIs(c.suite, sloppy)
        self.assertEqual(
            str(sloppy),
            debian_info.stable() + '-backports-sloppy')
        self.assertEqual(sloppy.hierarchy[0], sloppy)
        self.assertEqual(str(sloppy.hierarchy[1]), str(backports))
        self.assertEqual(str(sloppy.hierarchy[2]), str(stable))
        self.assertEqual(
            sloppy.sbuild_resolver,
            ['--build-dep-resolver=aptitude'])
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(sloppy),
            'http://192.168.122.1:3142/debian')
        self.assertEqual(sloppy.archive, 'debian')

        self.assertEqual(
            c.sbuild_resolver,
            ['--build-dep-resolver=aptitude'])

    def test_debian_stable_security(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'debian')
        c.override('suite', 'stable-security')

        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        debian = c.get_vendor('debian')
        self.assertIs(c.vendor, debian)

        debian_info = distro_info.DebianDistroInfo()
        security = c.ensure_suite(debian, 'stable-security')
        stable = c.ensure_suite(debian, 'stable')

        self.assertEqual(
            security.apt_suite,
            '{}-security'.format(debian_info.stable()))
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(security),
            'http://192.168.122.1:3142/debian-security')
        self.assertEqual(security.archive, 'debian-security')
        self.assertEqual(security.hierarchy[0], security)
        self.assertEqual(str(security.hierarchy[1]), str(stable))

        with self.assertRaises(AttributeError):
            c.archive       # type: ignore

    def test_debian_buster_security(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'debian')
        c.override('suite', 'buster-security')

        debian = c.get_vendor('debian')
        self.assertIs(c.vendor, debian)

        buster = c.ensure_suite(debian, 'buster')
        sec = c.ensure_suite(debian, 'buster-security')
        self.assertEqual(list(buster.hierarchy), [buster])
        self.assertEqual(list(sec.hierarchy), [sec, buster])
        self.assertIs(c.suite, sec)

        self.assertEqual(sec.apt_suite, 'buster/updates')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(sec),
            'http://192.168.122.1:3142/debian-security')
        self.assertEqual(sec.archive, 'debian-security')
        self.assertEqual(sec.hierarchy[0], sec)
        self.assertEqual(str(sec.hierarchy[1]), str('buster'))

    def test_debian_bullseye_security(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'debian')
        c.override('suite', 'bullseye-security')

        debian = c.get_vendor('debian')
        self.assertIs(c.vendor, debian)

        bullseye = c.ensure_suite(debian, 'bullseye')
        sec = c.ensure_suite(debian, 'bullseye-security')
        self.assertEqual(list(bullseye.hierarchy), [bullseye])
        self.assertEqual(list(sec.hierarchy), [sec, bullseye])
        self.assertIs(c.suite, sec)

        self.assertEqual(sec.apt_suite, 'bullseye-security')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(sec),
            'http://192.168.122.1:3142/debian-security')
        self.assertEqual(sec.archive, 'debian-security')
        self.assertEqual(sec.hierarchy[0], sec)
        self.assertEqual(str(sec.hierarchy[1]), str('bullseye'))

    def test_ubuntu(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'ubuntu')
        ubuntu = c.get_vendor('ubuntu')

        self.assertIs(c.vendor, ubuntu)

        self.assertEqual(str(ubuntu), 'ubuntu')
        self.assertIsNone(c.get_existing_suite(ubuntu, 'unstable'))
        self.assertIsNone(c.get_existing_suite(ubuntu, 'stable'))

        self.assertEqual(c.components, {'main', 'universe'})
        self.assertEqual(
            c.extra_components,
            {'restricted', 'multiverse'})
        self.assertEqual(
            c.all_components,
            {'main', 'universe', 'restricted', 'multiverse'})
        self.assertIs(c.vendor, ubuntu)
        self.assertIs(c.worker_vendor, ubuntu)
        self.assertIs(c.pbuilder_worker_vendor, ubuntu)
        self.assertIs(c.sbuild_worker_vendor, ubuntu)
        self.assertIs(c.vmdebootstrap_worker_vendor, ubuntu)
        with self.assertRaises(AttributeError):
            c.archive       # type: ignore
        self.assertEqual(c.autopkgtest, ['lxc', 'qemu'])
        self.assertEqual(c.components, {'main', 'universe'})
        self.assertEqual(
            c.extra_components,
            {'restricted', 'multiverse'})
        self.assertEqual(
            c.all_components,
            {'main', 'universe', 'restricted', 'multiverse'})
        self.assertIs(c.vendor, ubuntu)
        self.assertEqual(c.qemu_image_size, 32 * GiB)
        self.assertGreaterEqual(c.parallel, 1)
        self.assertIs(c.build_indep_together, False)
        self.assertIs(c.sbuild_source_together, False)
        self.assertEqual(c.sbuild_resolver, [])
        self.assertEqual(
            c.apt_key,
            '/usr/share/keyrings/ubuntu-archive-keyring.gpg')
        with self.assertRaises(AttributeError):
            c.apt_suite     # type: ignore
        self.assertIsNone(c.dpkg_source_diff_ignore)
        self.assertEqual(c.dpkg_source_tar_ignore, [])
        self.assertEqual(c.dpkg_source_extend_diff_ignore, [])
        self.assertEqual(c.output_parent, '..')

        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        ubuntu_info = distro_info.UbuntuDistroInfo()

        try:
            ubuntu_devel = ubuntu_info.devel()
        except distro_info.DistroDataOutdated:
            ubuntu_devel = ubuntu_info.stable()

        self.assertEqual(str(c.ensure_suite(ubuntu, 'devel')), ubuntu_devel)
        self.assertEqual(ubuntu.default_suite, ubuntu_devel)
        self.assertEqual(
            ubuntu.default_worker_suite,
            ubuntu_info.lts())
        devel = c.ensure_suite(ubuntu, 'devel')
        self.assertEqual(devel.archive, 'ubuntu')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(devel),
            'https://mirror/ubuntu')
        self.assertEqual(
            c.get_archive_access().apt_proxy_for_suite(devel),
            'http://sproxy:8080')

        lts = c.ensure_suite(ubuntu, ubuntu_info.lts())
        backports = c.ensure_suite(ubuntu, ubuntu_info.lts() + '-backports')
        self.assertIsInstance(backports, Suite)
        self.assertEqual(c.worker_suite, lts)
        self.assertEqual(c.pbuilder_worker_suite, lts)
        self.assertEqual(c.sbuild_worker_suite, lts)
        self.assertEqual(c.vmdebootstrap_worker_suite, lts)
        self.assertEqual(backports.archive, 'ubuntu')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(backports),
            'https://mirror/ubuntu')
        self.assertEqual(lts.archive, 'ubuntu')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(lts),
            'https://mirror/ubuntu')

    def test_ubuntu_xenial(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'ubuntu')
        c.override('suite', 'xenial')

        ubuntu = c.get_vendor('ubuntu')
        xenial = c.ensure_suite(ubuntu, 'xenial')
        self.assertEqual(list(xenial.hierarchy), [xenial])
        self.assertEqual(xenial.components, {'main', 'universe'})
        self.assertEqual(
            xenial.extra_components,
            {'multiverse', 'restricted'})
        self.assertEqual(
            xenial.all_components,
            {'main', 'universe', 'multiverse', 'restricted'})
        self.assertIs(xenial.base, None)
        self.assertEqual(xenial.archive, 'ubuntu')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(xenial),
            'https://mirror/ubuntu')
        self.assertEqual(
            xenial.apt_key,
            '/usr/share/keyrings/ubuntu-archive-keyring.gpg')
        self.assertEqual(xenial.apt_suite, 'xenial')

        self.assertEqual(c.components, {'main', 'universe'})
        self.assertEqual(
            c.extra_components,
            {'multiverse', 'restricted'})
        self.assertEqual(
            c.all_components,
            {'main', 'universe', 'multiverse', 'restricted'})
        self.assertIs(c.vendor, ubuntu)
        self.assertIs(c.worker_vendor, ubuntu)
        self.assertIs(c.pbuilder_worker_vendor, ubuntu)
        self.assertIs(c.sbuild_worker_vendor, ubuntu)
        self.assertIs(c.vmdebootstrap_worker_vendor, ubuntu)

        with self.assertRaises(AttributeError):
            c.archive       # type: ignore
        self.assertEqual(c.qemu_image_size, 32 * GiB)
        self.assertGreaterEqual(c.parallel, 1)
        self.assertIs(c.build_indep_together, False)
        self.assertIs(c.sbuild_source_together, False)
        self.assertEqual(c.sbuild_resolver, [])
        self.assertEqual(
            c.apt_key,
            '/usr/share/keyrings/ubuntu-archive-keyring.gpg')
        self.assertIsNone(c.dpkg_source_diff_ignore)
        self.assertEqual(c.dpkg_source_tar_ignore, [])
        self.assertEqual(c.dpkg_source_extend_diff_ignore, [])
        self.assertEqual(c.output_parent, '..')
        self.assertEqual(c.debootstrap_script, 'xenial')
        self.assertIs(c.suite, xenial)

        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        ubuntu_info = distro_info.UbuntuDistroInfo()
        lts = c.ensure_suite(ubuntu, ubuntu_info.lts())
        backports = c.ensure_suite(ubuntu, ubuntu_info.lts() + '-backports')
        self.assertIsInstance(backports, Suite)
        self.assertIs(c.worker_suite, lts)
        self.assertIs(c.pbuilder_worker_suite, lts)
        self.assertIs(c.sbuild_worker_suite, lts)
        self.assertIs(c.vmdebootstrap_worker_suite, lts)

        try:
            ubuntu_devel = ubuntu_info.devel()
        except distro_info.DistroDataOutdated:
            ubuntu_devel = ubuntu_info.stable()

        # FIXME: this seems wrong
        self.assertEqual(c.default_suite, ubuntu_devel)

    def test_ubuntu_xenial_security(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'ubuntu')
        c.override('suite', 'xenial-security')

        ubuntu = c.get_vendor('ubuntu')
        sec = c.ensure_suite(ubuntu, 'xenial-security')
        xenial = c.ensure_suite(ubuntu, 'xenial')
        self.assertEqual(list(sec.hierarchy), [sec, xenial])
        self.assertIs(sec.base, xenial)
        self.assertEqual(sec.components, {'main', 'universe'})
        self.assertEqual(
            sec.extra_components,
            {'multiverse', 'restricted'})
        self.assertEqual(
            sec.all_components,
            {'main', 'universe', 'multiverse', 'restricted'})
        self.assertEqual(sec.archive, 'ubuntu')
        self.assertEqual(
            c.get_archive_access().mirror_for_suite(sec),
            'https://mirror/ubuntu')
        self.assertEqual(
            sec.apt_key,
            '/usr/share/keyrings/ubuntu-archive-keyring.gpg')
        self.assertEqual(sec.apt_suite, 'xenial-security')

        with self.assertRaises(AttributeError):
            c.archive       # type: ignore
        self.assertEqual(c.qemu_image_size, 32 * GiB)
        self.assertGreaterEqual(c.parallel, 1)
        self.assertIs(c.build_indep_together, False)
        self.assertIs(c.sbuild_source_together, False)
        self.assertEqual(c.sbuild_resolver, [])
        self.assertEqual(
            c.apt_key,
            '/usr/share/keyrings/ubuntu-archive-keyring.gpg')
        self.assertIsNone(c.dpkg_source_diff_ignore)
        self.assertEqual(c.dpkg_source_tar_ignore, [])
        self.assertEqual(c.dpkg_source_extend_diff_ignore, [])
        self.assertEqual(c.output_parent, '..')
        self.assertEqual(c.debootstrap_script, 'xenial-security')
        self.assertIs(c.suite, sec)

        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        ubuntu_info = distro_info.UbuntuDistroInfo()
        backports = c.ensure_suite(ubuntu, ubuntu_info.lts() + '-backports')
        self.assertIsInstance(backports, Suite)
        lts = c.ensure_suite(ubuntu, ubuntu_info.lts())
        self.assertIs(c.worker_suite, lts)
        self.assertIs(c.pbuilder_worker_suite, lts)
        self.assertIs(c.sbuild_worker_suite, lts)
        self.assertIs(c.vmdebootstrap_worker_suite, lts)

    def test_unknown_vendor(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'steamos')
        c.override('suite', 'brewmaster')

        steamos = c.get_vendor('steamos')
        debian = c.get_vendor('debian')
        brewmaster = c.ensure_suite(steamos, 'brewmaster')

        self.assertEqual(str(steamos), 'steamos')
        self.assertEqual(steamos.components, {'main'})
        self.assertEqual(list(brewmaster.hierarchy), [brewmaster])
        with self.assertRaises(AttributeError):
            steamos.archive     # type: ignore

        self.assertEqual(c.components, {'main'})
        self.assertEqual(c.vendor, steamos)
        self.assertIs(c.worker_vendor, debian)
        self.assertIs(c.pbuilder_worker_vendor, debian)
        self.assertIs(c.sbuild_worker_vendor, debian)
        self.assertIs(c.vmdebootstrap_worker_vendor, debian)
        with self.assertRaises(AttributeError):
            c.archive       # type: ignore
        self.assertEqual(c.autopkgtest, [
            'schroot', 'qemu:autopkgtest.qcow2',
        ])

        self.assertIsNone(c.get_existing_suite(steamos, 'xyzzy'))
        self.assertIsNotNone(c.ensure_suite(steamos, 'xyzzy'))
        self.assertIs(
            c.ensure_suite(steamos, 'xyzzy'),
            c.ensure_suite(steamos, 'xyzzy'))

        self.assertEqual(
            c.get_archive_access().mirror_for_suite(brewmaster),
            'http://localhost/steamos')
        self.assertEqual(
            c.get_archive_access().apt_proxy_for_suite(brewmaster),
            'http://proxy:8080')
        self.assertEqual(brewmaster.archive, 'steamos')

        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        debian_info = distro_info.DebianDistroInfo()
        self.assertIs(
            c.worker_suite,
            c.ensure_suite(debian, debian_info.stable()))

    def test_cross_vendor(self) -> None:
        c = Arguments('test', self.__config)
        c.override('vendor', 'steamrt')
        c.override('suite', 'scout')

        steamrt = c.get_vendor('steamrt')
        ubuntu = c.get_vendor('ubuntu')
        debian = c.get_vendor('debian')
        scout = c.ensure_suite(steamrt, 'scout')
        precise = c.ensure_suite(ubuntu, 'precise')

        self.assertEqual(list(scout.hierarchy), [scout, precise])

        self.assertEqual(c.components, {'main', 'contrib', 'non-free'})
        self.assertEqual(c.vendor, steamrt)

        self.assertIs(c.worker_vendor, debian)
        self.assertIs(c.pbuilder_worker_vendor, debian)
        self.assertIs(c.sbuild_worker_vendor, debian)
        self.assertIs(c.vmdebootstrap_worker_vendor, debian)

        self.assertEqual(c.autopkgtest, [
            'schroot', 'qemu:autopkgtest.qcow2',
        ])

        self.assertEqual(
            c.get_archive_access().mirror_for_suite(scout),
            'http://192.168.122.1:3142/repo.steamstatic.com/steamrt')
        self.assertEqual(
            c.get_archive_access().apt_proxy_for_suite(scout),
            'DIRECT')
        self.assertEqual(scout.archive, 'repo.steamstatic.com/steamrt')

        try:
            import distro_info
            distro_info         # placate pyflakes
        except ImportError:
            return

        debian_info = distro_info.DebianDistroInfo()
        self.assertIs(
            c.worker_suite,
            c.ensure_suite(debian, debian_info.stable()))

    def tearDown(self) -> None:
        pass


if __name__ == '__main__':
    import tap
    runner = tap.TAPTestRunner()
    runner.set_stream(True)
    unittest.main(verbosity=2, testRunner=runner)
