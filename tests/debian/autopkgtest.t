#!/bin/sh
# vim:set ft=sh sw=4 sts=4 et:
# Copyright © 2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

set -e
set -u
set -x

if [ -n "${VECTIS_UNINSTALLED:-}" ]; then
    VECTIS="${PYTHON:-python3} ${VECTIS_UNINSTALLED}/run"
else
    VECTIS=vectis
fi

: "${XDG_CACHE_HOME:="${HOME}/.cache"}"
arch="$(dpkg --print-architecture)"

if ! stable="$(debian-distro-info --stable)"; then
    echo "1..0 # SKIP Could not determine current Debian stable suite"
    exit 0
fi

if ! testing="$(debian-distro-info --testing)"; then
    echo "1..0 # SKIP Could not determine current Debian testing suite"
    exit 0
fi

# Currently used for the sbuild worker
if ! [ -f "${XDG_CACHE_HOME}/vectis/debian/${stable}/${arch}/autopkgtest.qcow2" ]; then
    echo "1..0 # SKIP debian/${stable}/${arch}/autopkgtest.qcow2 not found"
    exit 0
fi

if ! [ -f "${XDG_CACHE_HOME}/vectis/debian/${testing}/${arch}/autopkgtest.qcow2" ]; then
    echo "1..0 # SKIP debian/${testing}/${arch}/autopkgtest.qcow2 not found"
    exit 0
fi

if ! [ -f "${XDG_CACHE_HOME}/vectis/debian/${testing}/${arch}/minbase.tar.gz" ]; then
    echo "1..0 # SKIP debian/${testing}/${arch}/minbase.tar.gz not found"
    exit 0
fi

if [ -z "${VECTIS_TEST_PROXY:-}" ]; then
    echo "1..0 # SKIP This test requires VECTIS_TEST_PROXY=http://192.168.122.1:3142 or similar"
    exit 0
fi

storage="$(mktemp --tmpdir -d vectis-test-XXXXXXXXXX)"

mkdir -p "${storage}/debian/${stable}/${arch}"
mkdir -p "${storage}/debian/${testing}/${arch}"
ln -s "${XDG_CACHE_HOME}/vectis/debian/${stable}/${arch}/autopkgtest.qcow2" "${storage}/debian/${stable}/${arch}/"
ln -s "${XDG_CACHE_HOME}/vectis/debian/${testing}/${arch}/autopkgtest.qcow2" "${storage}/debian/${testing}/${arch}/"
ln -s "${XDG_CACHE_HOME}/vectis/debian/${testing}/${arch}/minbase.tar.gz" "${storage}/debian/${testing}/${arch}/"

echo "1..1"

$VECTIS --vendor=debian --storage="${storage}" autopkgtest \
    --http-proxy="$VECTIS_TEST_PROXY" \
    --suite="${testing}" grep >&2
rm -fr "${storage}"

echo "ok 1"
